@echo off
rem Version 1.2 of batch file to run Platypus (c) Andrew Binstock 2016

rem Replace install_directory below with the complete name of the Platypus installation directory. Do not add a slash at end.
rem If the directory contains spaces, it should be enclosed in "quotes."

set PLATYPUS_HOME=install_directory 
java -Xmx256M -jar "%PLATYPUS_HOME%\platypus.jar" %1 %2 %3 %4 %5 %6 %7 %8