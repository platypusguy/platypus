/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.pz.platypus.mocks.MockLiterals;

/**
 * @author alb
 */
public class CommandTableTest
{
    private CommandTable ct;
    private GDD gdd;

    @Before
    public void setUp()
    {
        gdd = new GDD();
        gdd.setupGdd( new MockLiterals() );
    }

    @Test
    public void testConstructor()
    {
        ct = new CommandTable( gdd );
        assertEquals( 0, ct.size() );
    }
}
