/***
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2008-16 Andrew Binstock. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 * Test of copyrightNotice Class
 * @author alb
 *
 */
public class CopyrightTest
{
    private CopyrightNotice cn;

    @Before
    public void setUp()
    {
        cn = new CopyrightNotice();
    }

    @Test
    public void testConstructor()
    {
        assertTrue( cn.toString().startsWith( "© 2009" ));
        assertTrue( cn.toString().contains( "Andrew Binstock" ));
    }

    @Test
    public void testComputeCopyrightRange()
    {
    	String s = cn.computeCopyrightDateRange();
    	assertTrue( s.startsWith( "2009-1" ));
    }
}
