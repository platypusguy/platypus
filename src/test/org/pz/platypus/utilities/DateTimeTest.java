/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.utilities;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.Calendar;

/**
 * @author alb
 * Test the DateTime utility class
 */
public class DateTimeTest
{
    DateTime dt;

    @Test
    public void testConstructor()
    {
        dt = new DateTime();
        assertTrue(dt.getYear() > 2009);
        assertEquals(Calendar.getInstance().get( Calendar.YEAR ), dt.getYear() );
    }
}
