/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.utilities;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author alb
 * Test the DateTime utility class
 */
public class FilenameTest
{
    DateTime dt;

    @Test
    public void testGetExtensionOfValidFile()
    {
        assertEquals( Filename.getExtension( "abc.def.ghi.jkl" ), "jkl" );
    }

    @Test
    public void testGetExtensionFilenameHavingNoExtension()
    {
        assertTrue( Filename.getExtension( "filename" ).isEmpty() );
    }

    @Test
    public void testGetNameWithEmptyFilename()
    {
        assertTrue( Filename.getExtension( "" ).isEmpty() );
    }

    @Test
    public void testGetExtensionWithNullFilename()
    {
        assertTrue( Filename.getExtension( null ).isEmpty() );
    }

    @Test
    public void testGetFilenameWithoutExtensionForFilenameWithExtension()
    {
        assertEquals( Filename.getFilenameMinusExtension( "abc.def.ghi.jkl" ), "abc.def.ghi" );
    }

    @Test
    public void testGetFilenameWithoutExtensionForFilenameWithNoExtension()
    {
        assertEquals( Filename.getFilenameMinusExtension( "filename" ), "filename" );
    }

    @Test
    public void testGetFilenameWithoutExtensionForEmptyFilename()
    {
        assertTrue( Filename.getFilenameMinusExtension( "" ).isEmpty() );
    }

    @Test( expected = NullPointerException.class )
    public void testGetFilenameWithoutExtensionForNullFilename()
    {
        assertTrue( Filename.getFilenameMinusExtension( null ).isEmpty() );
    }

    //=== tests of getBaseName() ===//
    @Test( expected = NullPointerException.class )
    public void testNullforBaseName()
    {
        assertEquals( null, Filename.getBaseName( null ));
    }

    @Test
    public void testBaseNameOnlyFilename()
    {
        assertEquals( "basename", Filename.getBaseName( "basename" ));
    }

    @Test
    public void testBaseNameWithExtension()
    {
        assertEquals( "basename", Filename.getBaseName( "basename.ext" ));
    }

    @Test
    public void testBaseNameWithEmbeddedDotAndExtension()
    {
        assertEquals( "basename.abc", Filename.getBaseName( "basename.abc.ext" ));
    }

    @Test
    public void testBaseNameWithWindowsPathAndExtension()
    {
        assertEquals( "basename", Filename.getBaseName( "c:\\dir1\\dir2\\basename.ext" ));
    }

    @Test
    public void testBaseNameWithLinuxPathAndExtension()
    {
        assertEquals( "basename", Filename.getBaseName( "/home/dir1/dir2/basename.ext" ));
    }

    @Test
    public void testBaseNameWithLinuxAndWindowsPathsAndExtension()
    {
        assertEquals( "basename", Filename.getBaseName( "/home/dir1\\dir2/basename.ext" ));
    }

    @Test
    public void testBaseNameWithLinuxAndWindowsPathsAndNoExtension()
    {
        assertEquals( "basename", Filename.getBaseName( "/home/dir1\\dir2/basename" ));
    }
}
