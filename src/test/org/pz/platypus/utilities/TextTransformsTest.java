/***
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2006-09 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus.utilities;

import static org.junit.Assert.*;
import org.junit.Test;


/**
 * Test various text transformations
 *
 * @author alb
 */
public class TextTransformsTest
{
    @Test
    public void testCharArrayToStringFullArrayValid1()
    {
        char[] chars = { 'a', 'b', 'c' };
        String str = TextTransforms.charArrayToString( chars );
        assertEquals( "abc", str );
    }

    @Test
    public void testEmptyCharArray()
    {
        char[] chars = {};
        String str = TextTransforms.charArrayToString( chars );
        assertTrue( str.isEmpty() );
    }

    @Test
    public void testNullCharArray1()
    {
        String str = TextTransforms.charArrayToString( null );
        assertNull( str );
    }

    @Test
    public void testInvalidStartEnd1()
    {
        char[] chars = { 'a', 'b', 'c' };
        String str = TextTransforms.charArrayToString( chars, 0, 3 );
        assertNull( str );
    }

    @Test
    public void testInvalidStartEnd2()
    {
        char[] chars = { 'a', 'b', 'c' };
        String str = TextTransforms.charArrayToString( chars, 0, 0 );
        assertEquals( "a", str );
    }

    @Test
    public void testLopValid()
    {
        String test = "abcdefghi";
        String s = TextTransforms.lop( test, 3 );
        assertEquals( test.substring( 3, 9 ), s );
    }

    @Test
    public void testLopInvalid()
    {
        String test = "abcdefghi";
        String s = TextTransforms.lop( test, 300 );
        assertEquals( test, s );
    }

    @Test
    public void testLopNull()
    {
        String s = TextTransforms.lop( null, 4 );
        assertEquals( null, s );
    }

   @Test
    public void testTruncateValid()
    {
        String test = "abcdefghi";
        String s = TextTransforms.truncate( test, 3 );
        assertEquals( test.substring( 0, 6 ), s );
    }

    @Test
    public void testTruncateInvalid()
    {
        String test = "abcdefghi";
        String s = TextTransforms.truncate( test, 300 );
        assertEquals( test, s );
    }

    @Test
    public void testTruncateNull()
    {
        String s = TextTransforms.truncate( null, 4 );
        assertEquals( null, s );
    }

    @Test
    public void testReplaceSubstring()
    {
        String base = "abcxxxhij";
        String oldSubstring = "xxx";
        String newSubstring = "defg";
        String s = TextTransforms.replaceSubstringAtLocation(
                base, oldSubstring, newSubstring, 3 );
        assertEquals( "abcdefghij", s );
    }

    @Test
    public void testReplaceSubstringAtStart()
    {
        String base = "xxxdefghij";
        String oldSubstring = "xxx";
        String newSubstring = "abc";
        String s = TextTransforms.replaceSubstringAtLocation(
                base, oldSubstring, newSubstring, 0 );
        assertEquals( "abcdefghij", s );
    }
}
