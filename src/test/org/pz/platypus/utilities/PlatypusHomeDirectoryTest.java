/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.utilities;

import org.junit.Test;


import static org.junit.Assert.*;

/**
 * @author alb
 */
public class PlatypusHomeDirectoryTest
{
    PlatypusHomeDirectory phd;

    @Test
    public void testConstructor()
    {
        String[] args = { "1", "2" };
        phd = new PlatypusHomeDirectory( args );
        assertNotNull( phd.get() );
    }

    @Test
    public void testCheckCurrentDir()
    {
        String[] args = { "1", "2" };
        String home = new PlatypusHomeDirectory( args ).checkCurrentDir();
        home = FileSeparator.append( home );
        assertNotNull( home );
        assertTrue( home.endsWith( FileSeparator.get() ));
    }

    @Test
    public void testCheckCommandLine()
    {
        String[] args = { "-home", "c:/allo" };
        String[] argsUnused = { "1", "2" };
        String home = new PlatypusHomeDirectory( argsUnused ).checkCommandLine( args );
        assertNotNull( home );
        assertEquals( "c:/allo", home );
    }

    @Test
    public void testCheckCommandLineNoArg()
    {
        String[] args = { "1", "2" };
        String home = new PlatypusHomeDirectory( args ).checkCommandLine( args );
        assertNull( home );
    }

    @Test
    public void testValidateHomeNull()
    {
        String[] args = { "1", "2" };
        String home = new PlatypusHomeDirectory( args ).validateHome( null );
        assertNull( home );
    }
}
