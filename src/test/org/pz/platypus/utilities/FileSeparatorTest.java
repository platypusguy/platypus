/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.utilities;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author alb
 * Test the FileSeparator utility class
 */
public class FileSeparatorTest
{
    String fs;

    @Test
    public void testGet()
    {
        fs = FileSeparator.get();
        assertNotNull( fs );
        assertEquals( System.getProperty( "file.separator"), fs );
    }

    @Test
    public void testAppendValid1()
    {
        String dir = "c:/argyle";
        fs = FileSeparator.get();
        String s = FileSeparator.append( dir );
        assertTrue( s.endsWith( fs ));
        assertTrue( s.startsWith( dir ));
    }

    @Test
    public void testAppendValid2()
    {
        fs = FileSeparator.get();
        String dir = "c:/argyle" + fs;

        String s = FileSeparator.append( dir );
        assertTrue( s.endsWith( fs ));
        assertTrue( s.startsWith( dir ));
    }

    @Test
    public void testAppendNull()
    {
        String s = FileSeparator.append( null );
        assertEquals( null, s);
    }
}
