/***
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2010-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus.plugins.fontlist;

import com.google.common.io.Files;
import org.junit.Before;
import org.junit.Test;
import org.pz.platypus.GDD;
import org.pz.platypus.Literals;
import org.pz.platypus.mocks.MockLiterals;
import org.pz.platypus.utilities.TextTransforms;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Test of the capability to read fontdir.txt files, which contain a list
 * of directories (one to a line) that contain fonts that Platypus should
 * include when making up fontlist.txt.
 */

public class FontDirectoriesFileTest
{
    /** the GDD. Used to obtain the PLATYPUS_HOME directory */
    private GDD gdd;


    @Before
    public void setUp()
    {
        gdd = new GDD();
        gdd.initialize();

        Literals lits = new MockLiterals();
        gdd.setupGdd( lits );
    }

    @Test( expected = NullPointerException.class )
    public void invalidConstructorNull1()
    {
        FontDirectoriesFile fdf = new FontDirectoriesFile( null, gdd );
    }

    @Test( expected = NullPointerException.class )
    public void invalidConstructorNull2()
    {
        FontDirectoriesFile fdf = new FontDirectoriesFile( "filename", null );
    }

    @Test( expected = IllegalArgumentException.class )
    public void invalidConstructorEmptyFilename()
    {
        FontDirectoriesFile fdf = new FontDirectoriesFile( "", gdd );
    }

    @Test
    public  void testAddFontDirWithNoEndingSlash()
    {
        // get the name of the current dir and create a font directory file with it as an entry
        String dirName;
        String currDirName;
        File currDir = new File( "." );

        try {
            currDirName = currDir.getCanonicalPath();
        }
        catch( Exception e ) {
            fail( "could not obtain name of current directory" );
            return;
        }

        if( currDirName.endsWith( "/" ) || currDirName.endsWith( "\\" )) {
            dirName = TextTransforms.truncate( currDirName, 1 );
        }
        else {
            dirName = currDirName;
        }

        // write the name to a text file
        final String fileName = "FontDirectoriesFileTest.txt";
        final File newFile = new File( fileName );
        try
        {
            Files.write( dirName.getBytes(), newFile );
        }
        catch (IOException fileIoEx)
        {
            fail("Failure in Unit Testing: error writing test file in FontDirectoriesFileTest.java");
        }

        // now run the test
        FontDirectoriesFile fdf = new FontDirectoriesFile( fileName, gdd );
        FontDirectoriesEntries dirs = fdf.extractFontDirectories();
        assertEquals( 1, dirs.size() );
        assertEquals( dirName, dirs.get( 0 ));

        // clean up the test file.
        newFile.delete();
    }

    // add test with ending slash in directory name.
}
