/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus;

import org.junit.Before;
import org.junit.Test;
import org.pz.platypus.exceptions.InvalidConfigFileException;
import org.pz.platypus.mocks.MockLiterals;

import java.util.Enumeration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * @author alb
 */
public class CommandTableLoaderTest
{
    private CommandTableLoader ctl;
    private GDD gdd;

    @Before
    public void setUp()
    {
        gdd = new GDD();
        gdd.setupGdd( new MockLiterals() );
        ctl = new CommandTableLoader( gdd );
    }

    @Test (expected = NullPointerException.class )
    public void testLoadCommandsInvalidNull()
    {
        ctl = new CommandTableLoader( gdd );
        ctl.load( null );
    }

    @Test (expected = InvalidConfigFileException.class )
    public void testLoadCommandsInvalidFile()
    {
        ctl = new CommandTableLoader( gdd );
        ctl.load( "" );
    }

    @Test
    public void testValidLoadOf2Recs()
    {
        PropertyFile pf = new PropertyFile( "oktodelete.properties" );
        pf.testOnlyAdd( "key1", "value1" );
        pf.testOnlyAdd( "key2", "value2" );
        assertEquals( 2, pf.getSize() );
    }

    @Test
    public void testValidLoadOfAllPlatyCommandTypes()
    {
        PropertyFile pf = new PropertyFile( "oktodelete.properties" );
        pf.testOnlyAdd( "[align:",  "sn" );
        pf.testOnlyAdd( "[code]",   "0n" );
        pf.testOnlyAdd( "[columns:","vn" );
        pf.testOnlyAdd( "[fsize:",  "r [font|size:" );
        assertEquals( 4, pf.getSize() );
        assertEquals( "sn", pf.lookup( "[align:" ));
        assertEquals( " ", pf.lookup( "nosuchcommand" ));
    }

    @Test
    public void testExtractionOfContents()
    {
        PropertyFile pf = new PropertyFile( "oktodelete.properties" );
        pf.testOnlyAdd( "[align:",  "sn" );
        pf.testOnlyAdd( "[code]",   "0n" );
        pf.testOnlyAdd( "[columns:","vn" );
        pf.testOnlyAdd( "[fsize:",  "r [font|size:" );

        Enumeration<Object> contents = pf.getKeys();
        if( contents.hasMoreElements() ) {
            String s = (String) contents.nextElement();
            assertTrue( s.endsWith( ":" ) || s.endsWith( "]" ));
        }
        else {
            fail( "enumeration of command file contains no commands in CommandTableLoaderTest.java");
        }
    }

    @Test
    public void testCommandsLoading()
    {
        PropertyFile pf = new PropertyFile( "oktodelete.properties" );
        pf.testOnlyAdd( "[align:",  "sn" );
        pf.testOnlyAdd( "[code]",   "0n" );
        pf.testOnlyAdd( "[columns:","vn" );
        pf.testOnlyAdd( "[fsize:",  "r [font|size:" );

        Enumeration<Object> contents = pf.getKeys();
        ctl.loadCommands( contents, pf );

        assertEquals( 4, gdd.commandTable.size() );
    }


}
