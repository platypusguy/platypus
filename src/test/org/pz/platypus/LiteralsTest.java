/***
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2008-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.pz.platypus.mocks.MockLiterals;

import java.util.MissingResourceException;
import java.util.logging.Level;

public class LiteralsTest
{
    private Literals lits;
    private GDD gdd;

    @Before
    public void setUp()
    {
        MockLiterals mockLits = new MockLiterals( "Platypus.properties" );
        mockLits. setGetLitShouldReturnValue( true );
        lits = mockLits;

        gdd = new GDD();
        gdd.setupGdd( lits );
        gdd.log.setLevel( Level.OFF );
    }

    @Test
    public void testGetLiterals()
    {
        assertNotNull( lits );
    }

    @Test
    public void testGetExistingLiteralWrongCase()
    {
        String testStr = lits.getLit( "error.colon" );
        assertEquals( testStr, " " );
    }

    @Test
    public void testGetNonExistentLiteral()
    {
        String testStr = lits.getLit( "(((&*$%##(((((" );
        assertEquals( testStr, " " );
    }

    @Test
    public void testGetNullLiteral()
    {
        Literals l = new Literals();
        String testStr = l.getLit( null );
        assertEquals( testStr, " " );
    }

    @Test(expected = MissingResourceException.class)
    public void testConstuctorWithInvalidPath()
    {
        // should throw an exception
        gdd.homeDirectory = "non-existent directory/";
        Literals l = new Literals( gdd );
    }
}

