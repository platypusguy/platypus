/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus;

import org.junit.*;
import org.pz.platypus.exceptions.StopExecutionException;
import org.pz.platypus.mocks.MockLiterals;
import org.pz.platypus.utilities.PlatypusHomeDirectory;

import java.util.logging.Level;
import java.net.*;

import static org.junit.Assert.*;

/**
 * @author alb
 */
public class PluginLoaderTest
{
    PluginLoader pil;
    GDD gdd;

    @Before
    public void setUp()
    {
        Literals lits;

        String args[] = { "" };
        PlatypusHomeDirectory homeDir = new PlatypusHomeDirectory( args );
        gdd = new GDD( homeDir.get() );

        MockLiterals mockLits = new MockLiterals( "Platypus.properties" );
        mockLits. setGetLitShouldReturnValue( true );
        lits = mockLits;
        gdd.setupGdd( lits );
        gdd.log.setLevel( Level.OFF );
    }

    @Test(expected = NullPointerException.class)
    public void testConstructorInvalidGdd()
    {
        pil = new PluginLoader( ".", null );
    }

    @Test( expected = NullPointerException.class )
    public void testNullPluginrun()
    {
        pil = new PluginLoader( null, gdd );
        pil.runPlugin();
    }

    @Test( expected = StopExecutionException.class )
    public void testNonExistentPluginrun()
    {
        pil = new PluginLoader( "no.such.jar", gdd );
        pil.runPlugin();
    }

    //TODO: UATs for the other exceptions. They'll need extensive set up. Not sure it's worth it.

//    @Test( expected = StopExecutionException.class ) // this test results in ClassNotFound error, which
                                                       // is already triggered by testNonExistentPluginrun()
//    public void testInvalidJarPluginrun()
//    {
//        String filename = "invalid.jar";
//
//        try {
//            new File( filename ).createNewFile();
//        } catch (IOException e) {
//            fail( "***ERROR OCCURRED IN PluginLoaderTest.java***" );
//        }
//        gdd.log.setLevel( Level.ALL );
//        pil = new PluginLoader( filename, gdd );
//        pil.runPlugin();
//    }

    @Test
    public void testValidCreatePluginUrl()
    {
        pil = new PluginLoader( "plugin.mock", gdd );
        URL urlForPlugin = pil.createPluginUrl();
        assertTrue( urlForPlugin.getFile().endsWith( "plugin.mock" ));
    }

    /* Note: Because createPluginUrl() adds the string "file:" before the file name, it appears
       impossible to force a MalformedException. Consequently, no extraordinary efforts are made
       to reproduce one here. Note: Even passing null as the location does not trigger the exception.
     */
}
