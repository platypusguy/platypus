/***
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2006-08 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus.parser;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.pz.platypus.*;
import org.pz.platypus.mocks.MockLiterals;

import java.util.logging.Level;

/**
 * Test the macro parser
 *
 * @author alb
 */
public class MacroParserTest
{
    private GDD gdd;
    private MockLiterals lits;
    private TokenList tl;
    private MacroParser mp;

    @Before
    public void setUp()
    {
        gdd = new GDD();
        gdd.initialize();
        lits = new MockLiterals();
        gdd.setupGdd( lits );
        gdd.log.setLevel( Level.OFF );
        tl = new TokenList();
        mp = new MacroParser( gdd );
    }

    @Test
    public void testValidSystemMacro1()
    {
        gdd.sysStrings.add( "_version", lits.getLit( "VERSION" ));
        ParseContext context = new ParseContext( gdd, new Source(), "[$_version] hello\n", 0 );
        int len = mp.parse( context, tl );
        assertEquals( 1, tl.size() );
        assertEquals( TokenType.TEXT, tl.get( 0 ).getType() );
        assertEquals( lits.getLit( "VERSION" ), tl.get( 0 ).getContent() );
        assertEquals( "[$_version]".length(), len);
    }

    @Test
    public void testValidUserMacro1()
    {
        String rawMacro = "[$platypus]";
        ParseContext context = new ParseContext( gdd, new Source(), rawMacro + " hello\n", 0 );
        int len = mp.parse( context, tl );
        assertEquals( 1, tl.size() );
        assertEquals( TokenType.MACRO, tl.get( 0 ).getType() );
        assertEquals( "platypus", tl.get( 0 ).getContent() );
        assertEquals( rawMacro.length(), len);
    }

    @Test
    public void testLookupSystemMacroInvalid()
    {
        assertNull( mp.lookupSystemMacro( "fanerk" ));
    }

    @Test
    public void testLookupSystemMacroValid()
    {
        gdd.sysStrings.add( "_version", lits.getLit( "VERSION" ));
        assertTrue( lits.getLit( "VERSION" ).equals( mp.lookupSystemMacro( "_version" )));
    }

}
