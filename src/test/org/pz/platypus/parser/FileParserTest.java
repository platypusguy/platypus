/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.parser;

import org.junit.Before;
import org.pz.platypus.GDD;
import org.pz.platypus.Literals;
import org.pz.platypus.exceptions.StopExecutionException;
import org.pz.platypus.mocks.MockLiterals;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import static com.google.common.io.Files.*;

import static org.junit.Assert.*;
import org.junit.Test;
import org.pz.platypus.mocks.MockLiterals;

import java.util.MissingResourceException;
import java.util.logging.Level;

public class FileParserTest
{
    private Literals lits;
    private GDD gdd;
    private FileParser fp;

    @Before
    public void setUp()
    {
        MockLiterals mockLits = new MockLiterals( "Platypus.properties" );
        mockLits. setGetLitShouldReturnValue( true );
        lits = mockLits;

        gdd = new GDD();
        gdd.setupGdd( lits );
        gdd.log.setLevel( Level.OFF );
    }

    @Test( expected = NullPointerException.class )
    public void testNullFilename()
    {
        fp = new FileParser( null, gdd );
        fp.parse();
    }

    @Test( expected= StopExecutionException.class )
    public void testInvalidFileName()
    {
        fp = new FileParser( "muctor the non-existent", gdd );
        fp.parse();
    }

    @Test
    public void testEmptyInputFile()
    {
        TokenList tokens = null;
        File testFile = null;

        try {
            final String filename = "FileParsertTest.java.data";
            testFile = new File( filename );
            touch( testFile );  // this creates an empty file in Guava library
            fp = new FileParser( filename, gdd );
            tokens = fp.parse();
        }
        catch( IOException ioe ) {
            fail( "IOException occurred in FileParserTest.java" );
        }
        if( tokens == null ) {
            fail( "tokens returned in testEmptyInputFile were null in FileParserTest.java" );
        }
        assertTrue( tokens.isEmpty() );
        testFile.delete();
    }
}
