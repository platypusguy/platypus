/***
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2006-08 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus.parser;

import org.junit.Before;
import org.junit.Test;
import org.pz.platypus.GDD;
import org.pz.platypus.mocks.MockLiterals;

import java.security.InvalidParameterException;
import java.util.logging.Level;

import static org.junit.Assert.*;

/**
 * Test the macro parser
 *
 * @author alb
 */
public class MacroNameExtractorTest
{
    private GDD gdd;
    private MockLiterals lits;
    private TokenList tl;
    private MacroNameExtractor mne;
    private String name;

    @Before
    public void setUp()
    {
        gdd = new GDD();
        gdd.initialize();
        lits = new MockLiterals();
        gdd.setupGdd( lits );
        gdd.log.setLevel( Level.OFF );
        mne = new MacroNameExtractor();
        name = null;
    }

    @Test( expected = NullPointerException.class )
    public void testSetUp1()
    {
        name = mne.getName( null, 2, gdd );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testSetUp2()
    {
        name = mne.getName( "[$_version]".toCharArray(), 200, gdd );
    }

    @Test
    public void testValidSystemMacro1()
    {
        name = mne.getName( "[$_version]".toCharArray(), 2, gdd );
        assertEquals( "_version", name );
    }

    @Test( expected = IllegalArgumentException.class )
    public void testInvalidSystemMacro1()
    {
        name = mne.getName( "[$_version ]".toCharArray(), 2, gdd );
    }
}
