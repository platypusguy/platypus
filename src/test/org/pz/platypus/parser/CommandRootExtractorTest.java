/***
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2006-09 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus.parser;

import org.junit.Before;
import org.junit.Test;
import org.pz.platypus.GDD;
import org.pz.platypus.Literals;
import org.pz.platypus.logging.LogConsole;
import org.pz.platypus.mocks.MockLiterals;

import java.util.logging.Level;

import static org.junit.Assert.assertEquals;

/**
 * Test of CommandRootExtractor, which when given a string and a pointer to the first char in the
 * command, extracts the root for the command.
 *
 * @author alb
 */
public class CommandRootExtractorTest
{
    GDD gdd;
    CommandRootExtractor rootEx;
    ParseContext context;

    @Before
    public void setUp()
    {
        gdd = new GDD();
        gdd.initialize();

        Literals lits = new MockLiterals();
        gdd.setLits( lits );

        LogConsole logC =  new LogConsole( null, lits );
        gdd.log = logC;
        gdd.log.setLevel( Level.OFF );
        rootEx = new CommandRootExtractor( gdd );
    }


    @Test (expected = NullPointerException.class )
    public void invalidNullParameterToGet()
    {
        rootEx.getRoot( null );
    }

    @Test
    public void validGetSingleCommand1()
    {
        String root = rootEx.getRoot( new ParseContext( gdd, new Source( 1 ), "[+u]\n", 0 ));
        assertEquals( "[+u]", root );
    }

    @Test
    public void validGetCommand2()
    {
        String root = rootEx.getRoot( new ParseContext( gdd, new Source( 1 ), "[fsize:12pt]\n", 0 ));
        assertEquals( "[fsize:", root );
    }

    @Test
    public void validGetCommand3()
    {
        String root = rootEx.getRoot( new ParseContext( gdd, new Source( 1 ), "[fsize 12pt]\n", 0 ));
        assertEquals( "[fsize:", root );
    }

    @Test
    public void validGetCommandMacroDereference4()
    {
        String root = rootEx.getRoot( new ParseContext( gdd, new Source( 1 ), "[$DATE]\n", 0 ));
        assertEquals( "[$", root );
    }

    @Test
    public void validGetCommandPrint5()
    {
        String root = rootEx.getRoot( new ParseContext( gdd, new Source( 1 ), "[*{hello, hello!}]\n", 0 ));
        assertEquals( "[*", root );
    }

    @Test
    public void validGetCommandFamily6()
    {
        String root = rootEx.getRoot( new ParseContext( gdd, new Source( 1 ), "[font|size=12pt]\n", 0 ));
        assertEquals( "[font|", root );
    }

    @Test( expected = IndexOutOfBoundsException.class )
    public void invalidGetCommandUnclosed()
    {
        String root = rootEx.getRoot( new ParseContext( gdd, new Source( 1 ), "\n", 0 ));
//        assertEquals( "[*", root );
    }
}
