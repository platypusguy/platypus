/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.parser;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.pz.platypus.ConfigFile;
import org.pz.platypus.GDD;
import org.pz.platypus.SystemStrings;
import org.pz.platypus.mocks.MockLiterals;

import java.util.logging.Level;

/**
 * @author alb
 */
public class LineParserTest
{
        private LineParser lp;
        private TokenList toks;
        private GDD gdd;
        private MockLiterals lits;

        @Before
        public void setUp()
        {
            gdd = new GDD();
            gdd.initialize();
            gdd.setupGdd(new MockLiterals());
            gdd.log.setLevel( Level.OFF );

            gdd.configFile = new ConfigFile();
            gdd.sysStrings = new SystemStrings();
            gdd.sysStrings.add( "_FORMAT", "unknown" );

//            CommandTable ct = new CommandTable( gdd );
//      Insert the following commands that are needed for testing the font family compound commands
//        [font|=vn
//        [font|face=sn
//        [font|size=vn
//        [fsize:=r [font|size:
//            ct.add( new CommandV( "[font|:", 'n' ));
//            ct.add( new CommandS( "[font|face:", 'n' ));
//            ct.add( new CommandV( "[font|size:", 'n' ));
//            ct.add( new CommandR( "[fsize:", "r [font|size:", ct ));
//            gdd.setCommandTable( ct );

            lp = new LineParser();
            toks = new TokenList();
        }

    @Test
    public void testBlankLine()   //should return one token consisting of a [CR] string.
    {
        final String eol = "\n";
        InputLine line = new InputLine( 0, 1, eol );
        ParserState pState = new ParserState();
        toks = lp.parseLine( line, pState,  gdd );

        assertEquals( 1, toks.size() );
        Token t = toks.get( 0 );
        assertEquals( "[CR]", t.getRoot() );
        assertEquals( TokenType.COMMAND, t.getType() );
    }

    @Test
    public void testBlockCommentStillOpen()   // in a block comment; line does not close the block comment,
    {                                         // so entire line should be a block-comment token.
        final String content = "this is part of a block comment";
        final String eoCommentBlock = "%%]";

        gdd.configFile.testOnlyAdd( "pi.out." + gdd.sysStrings.get( "_FORMAT" ) + ".keep_comments", "yes" );
        InputLine line = new InputLine( 0, 1, content + "\n" );
        ParserState pState = new ParserState();
        pState.closingCommentBlock = eoCommentBlock;
        toks = lp.parseLine( line, pState,  gdd );

        assertEquals( 2, toks.size());

        Token t1 = toks.get( 0 );
        assertEquals( TokenType.BLOCK_COMMENT, t1.getType() );
        assertEquals( content, t1.getContent() );

        Token t2 = toks.get( 1 );
        assertEquals( TokenType.COMMAND, t2.getType() );
        assertEquals( "[cr]", t2.getRoot() );
    }

    @Test
    public void testBlockCommentClosed()   // closing block comment found at end of line.
    {
        final String content = "this closes a block comment %%]";
        final String eoCommentBlock = "%%]";

        gdd.configFile.testOnlyAdd( "pi.out." + gdd.sysStrings.get( "_FORMAT" ) + ".keep_comments", "yes" );
        InputLine line = new InputLine( 0, 1, content + "\n" );
        ParserState pState = new ParserState();
        pState.closingCommentBlock = eoCommentBlock;
        toks = lp.parseLine( line, pState,  gdd );

        assertEquals( 2, toks.size());      // the end of block comment, and the [cr]
        Token t = toks.get( 0 );
        assertEquals( content, t.getContent() );
        assertEquals( TokenType.BLOCK_COMMENT, t.getType() );
    }

    @Test
    public void testBlockCommentStillOpenAndNoCommentsEmitted()   // in a block comment;
    {                                                             // not emitting comments, so token list s/be empty
        final String content = "this is part of a block comment";
        final String eoCommentBlock = "%%]";

        gdd.configFile.testOnlyAdd( "pi.out." + gdd.sysStrings.get( "_FORMAT" ) + ".keep_comments", "NO" );
        InputLine line = new InputLine( 0, 1, content + "\n" );
        ParserState pState = new ParserState();
        pState.closingCommentBlock = eoCommentBlock;
        toks = lp.parseLine( line, pState,  gdd );

        assertEquals( 0, toks.size());
    }

    @Test
    public void testFullLineInScript()
    {
        final String scriptLine = "in a script";
        InputLine line = new InputLine( 0, 1, scriptLine + "\n" );
        ParserState pState = new ParserState( null, false, true );  // show we're in a script.
        toks = lp.parseLine( line, pState,  gdd );

        assertEquals( 1, toks.size() );
        Token t = toks.get( 0 );
        assertEquals( TokenType.SCRIPT, t.getType());
        assertEquals( scriptLine + "\n", t.getContent() );
    }

    @Test
    public void testPartialLineInScript1()  // tests LineParser.parseSegment
    {                                       // currently parses to script, end-of-script marker.
        // TODO: Should add rest of text + [cr].
        final String scriptLine = "at end of script[<<] with extra text";
        InputLine line = new InputLine( 0, 1, scriptLine + "\n" );
        ParserState pState = new ParserState( null, false, true );  // show we're in a script.
        ParseContext pContext = new ParseContext( gdd, new Source( 0, 1 ), line.getContent(), 0 );
        lp.initialize( line, pState, gdd );
        int i = lp.parseSegment( pContext, pState );

        assertEquals( 2, lp.tokens.size() );

        Token t1 = lp.tokens.get( 0 );
        assertEquals( TokenType.SCRIPT, t1.getType());
        assertEquals( "at end of script", t1.getContent() );

        Token t2 = lp.tokens.get( 1 );
        assertEquals( TokenType.COMMAND, t2.getType());
        assertEquals( "[<<]", t2.getRoot() );

        assertEquals( "at end of script".length() + "[<<]".length(), i);
    }

    @Test
    public void testBlankLineInScript1()  // tests LineParser.parseSegment
    {
        InputLine line = new InputLine( 0, 1, "\n" );
        ParserState pState = new ParserState( null, false, true );  // show we're in a script.
        ParseContext pContext = new ParseContext( gdd, new Source( 0, 1 ), line.getContent(), 0 );
        lp.initialize( line, pState, gdd );
        int i = lp.parseSegment( pContext, pState );

        assertEquals( 1, lp.tokens.size() );

        Token t1 = lp.tokens.get( 0 );
        assertEquals( TokenType.SCRIPT, t1.getType());
        assertEquals( "\n", t1.getContent() );
    }
}
