/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.commandTypes;

import org.junit.Test;
import org.pz.platypus.GDD;
import org.pz.platypus.mocks.MockLiterals;
import org.pz.platypus.parser.*;

import static org.junit.Assert.assertEquals;

/**
 * Test of the Symbol class.
 * @author alb
 */
public class SymbolTest {

    private Symbol sym;

    @Test
    public void testConstructorValid()
    {
        sym = new Symbol( "[e^]" );
    };

    @Test(expected = NullPointerException.class )
    public void testConstructorInvalidNull()
    {
        sym = new Symbol( null );
    }

    @Test
    public void testGetRoot()
    {
        String root = "[U^]";
        sym = new Symbol( root );
        assertEquals( root,  sym.getRoot() );
    }

    @Test
    public void testProcessValid()
    {
        String root = "[U^]";
        sym = new Symbol( root );

        GDD gdd = new GDD();
        gdd.setupGdd( new MockLiterals() );

        TokenList tl = new TokenList();
        ParseContext context = new ParseContext( gdd, new Source( 1 ), root + "\n", 0 );
        int length = sym.process( gdd, context, tl, false );
        assertEquals( root.length(), length );
        assertEquals( 1, tl.size());
        assertEquals(TokenType.SYMBOL, tl.get( 0 ).getType());
    }
}