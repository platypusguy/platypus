/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.commandline;

import org.apache.commons.cli.CommandLine;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.pz.platypus.*;
import org.pz.platypus.exceptions.StopExecutionException;
import org.pz.platypus.mocks.MockLiterals;
import org.pz.platypus.utilities.PlatypusHomeDirectory;

import java.util.logging.Level;

import static org.junit.Assert.*;

/**
 * Tests the format selection in FormatSelection.java. Note, the class is principally
 * exercised by UATs in ConfigFileTest
 * @author alb
 */
public class FormatSelectionTest
{
    ClParser clp;
    FormatSelection fs;
    GDD gdd;


    @Before
    public void setUp()
    {
        Literals lits;

        String args[] = { "" };
        PlatypusHomeDirectory homeDir = new PlatypusHomeDirectory( args );
        gdd = new GDD( homeDir.get() );

        MockLiterals mockLits = new MockLiterals( "Platypus.properties" );
        mockLits. setGetLitShouldReturnValue( true );
        lits = mockLits;
        gdd.setupGdd( lits );
        gdd.log.setLevel( Level.OFF );
    }

    // if no input file specified and it's not -help or -fontlist, then
    // throw a fatal exception
    @Test(expected=StopExecutionException.class)
    public void testInvalidLackOfInputFile()
    {
        String[] args = new String[] {
            "-format", "html"
        };

        clp = new ClParser( args );
        CommandLine cli = clp.getLine();
        assertTrue( cli != null );

        ClProcessor clproc = new ClProcessor( args, gdd );
        String outputPlugin = clproc.process();
    }
}
