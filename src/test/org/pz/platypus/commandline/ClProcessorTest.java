/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-16 Andrew Binstock. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.commandline;

import org.junit.*;
import org.pz.platypus.*;
import org.pz.platypus.exceptions.StopExecutionException;
import org.pz.platypus.mocks.MockLiterals;
import org.pz.platypus.utilities.PlatypusHomeDirectory;

import java.util.logging.Level;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * @author alb
 */
public class ClProcessorTest
{
    GDD gdd;
    ClParser clp;
    ClProcessor clpro;

    @Before
    public void setUp()
    {
        String args[] = { "" };
        PlatypusHomeDirectory homeDir = new PlatypusHomeDirectory( args );
        gdd = new GDD( homeDir.get() );
        Literals lits = new MockLiterals();
        gdd.setupGdd( lits );
        gdd.log.setLevel( Level.OFF );
        gdd.configFile = new ConfigFile( new ConfigLocator( homeDir ).get(), gdd );
    }

    private void setupArgs( String[] args )
    {
        clp = new ClParser( args );
        clpro = new ClProcessor( args, gdd );
    }

    @Test
    public void testVerbose()
    {
        String[] args = new String[] { "input.txt", "output.pdf", "-verbose" };
        setupArgs( args );
        clpro.processBooleanOptions( clp.getLine() );
        assertTrue( gdd.clVerbose );
        assertTrue( ! gdd.clVVerbose );
    }

    @Test
    public void testVverbose()
    {

        String[] args = new String[] { "input.txt", "output.pdf", "-vverbose" };
        setupArgs( args );
         clpro.processBooleanOptions( clp.getLine() );
        assertTrue( gdd.clVerbose );
        assertTrue( gdd.clVVerbose );
    }

    @Test
    public void testNoCommandLineOptions()
    {
        String[] args = new String[] {};
        setupArgs( args );
        String outputPlugin = clpro.process();
        assertNull( outputPlugin );
    }

    @Test
    public void testAddCommandLineToSystemStrings()
    {
        String[] args = new String[] { "input.txt", "file output.pdf", "-verbose" };
        setupArgs( args );
        SystemStrings sysStrings = new SystemStrings();

        clpro.addCommandLineToSystemStrings( args, sysStrings );

        assertEquals( 1, sysStrings.getSize() );
        assertEquals( "input.txt \"file output.pdf\" -verbose", sysStrings.get( "_COMMAND_LINE" ));
    }
}
