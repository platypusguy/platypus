/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.commandline;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author alb
 */
public class ArgumentOptionsTest
{
    @Test
    public void testConstructor()
    {
        ArgumentOptions aos = new ArgumentOptions();
        assertTrue( aos.getLength() > 3 );
    }

    @Test
    public void testContents()
    {
        ArgumentOptions aos = new ArgumentOptions();
        ArgumentOption[] aoa = aos.getOptions();

        assertTrue( aoa[1].getName() != null );
        assertTrue( ! aoa[1].getName().isEmpty() );

        assertTrue( aoa[1].getDescription() != null );
        assertTrue( ! aoa[1].getDescription().isEmpty() );

        assertTrue( aoa[1].getSyntax() != null );
        assertTrue( ! aoa[1].getSyntax().isEmpty() );
    }
}
