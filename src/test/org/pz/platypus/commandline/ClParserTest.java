/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.commandline;

import org.apache.commons.cli.CommandLine;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author alb
 */
public class ClParserTest
{
    ClParser clp;

    @Before
    public void setUp()
    {
        String[] args = new String[] {
                "input.txt", "output.pdf", "-vverbose"
        };

        clp = new ClParser( args );
    }
    @Test
    public void testConstructor()
    {
        CommandLine cli = clp.getLine();
        assertTrue( cli != null );
    }

    @Test
    public void testCommandLineContents()
    {
        CommandLine cli = clp.getLine();
        assertTrue( cli != null );

        assertEquals( "input.txt", cli.getOptionValue( "infile" ));
        assertTrue( cli.hasOption( "vverbose" ));
    }

    @Test
    public void testEmptyCommandLine()
    {
        clp = new ClParser( null );
        CommandLine cli = clp.getLine();
        assertTrue( cli != null );

        assertTrue( cli.hasOption( "help" ));
    }
}
