/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.commandline;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author alb
 */
public class BooleanOptionsTest
{
    @Test
    public void testConstructor()
    {
        BooleanOptions bos = new BooleanOptions();
        assertTrue( bos.getLength() > 3 );
    }

    @Test
    public void testContents()
    {
        BooleanOptions bos = new BooleanOptions();
        BooleanOption[] boa = bos.getOptions();

        assertTrue( boa[1].getName() != null );
        assertTrue(!boa[1].getName().isEmpty());

        assertTrue( boa[1].getDescription() != null );
        assertTrue( ! boa[1].getDescription().isEmpty() );
    }
}
