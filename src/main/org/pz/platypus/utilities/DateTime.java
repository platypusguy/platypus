/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.utilities;

import java.util.Calendar;

/**
 * Handles Platypus date and time routines.
 *
 * @author alb
 */
public class DateTime
{
    private Calendar currDate;

    /**
     * Initialize to the current date and time
     */
    public DateTime()
    {
        update();
    }

    /**
     * Update to the current date and time
     */
    public void update()
    {
         currDate = Calendar.getInstance();
    }

    //==== getters ====//

    public int getYear()
    {
        return( currDate.get( Calendar.YEAR ));
    }
}
