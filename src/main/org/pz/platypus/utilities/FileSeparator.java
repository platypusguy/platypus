/***
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus.utilities;

/**
 * Miscellaneous routines for handling the file separator (in a path)
 *
 * @author alb
 */
public class FileSeparator
{
    private static final String fs = System.getProperty( "file.separator" );

    /**
     * Accepts a path and appends the file separator if it does not already have one.
     * @param path path to append separator to
     * @return the path with a single file separator on the end, null if path is null
     */
    public static String append( final String path )
    {
        if( path != null ) {
            if( path.endsWith( fs )) {
                return( path );
            }
            else {
                return( path + fs );
            }
        }
        return( path ); // only if path = null.
    }

    public static String get()
    {
        return( fs );
    }
}
