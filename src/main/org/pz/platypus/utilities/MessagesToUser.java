/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.utilities;

import org.pz.platypus.Literals;
import org.pz.platypus.commandline.ClParser;

import java.util.Map;
import java.util.TreeMap;

/**
 * Many messages to the user shown here.
 *
 * @author alb
 */
public class MessagesToUser
{
    public static void showUsage( final Literals lits, ClParser clp )
    {
        if( lits != null ) {
            System.out.println(
                    lits.getLit( "USAGE.GENERAL.1" ) + "\n" +
                    lits.getLit( "USAGE.GENERAL.2" ) + "\n" +
                    lits.getLit( "USAGE.GENERAL.3" ));

            TreeMap<String,String> allOptions = clp.getAllOptions();
            String currKey = "";
            Map.Entry<String,String> entry;
            for( int i=0; i < allOptions.size(); i++ ) {
                entry = allOptions.higherEntry( currKey );
                if( ! entry.getKey().startsWith( "-infile" ) && ! entry.getKey().startsWith( "-outfile" )) {
                    System.out.print( "\t" + entry.getKey() );
                    for( int j = entry.getKey().length(); j < 20; j++ )
                        System.out.print( " " );
                    System.out.println( entry.getValue() );
                }
                currKey = entry.getKey();
            }
        }
    }
}
