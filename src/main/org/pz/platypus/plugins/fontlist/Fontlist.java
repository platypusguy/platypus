/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-12 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.plugins.fontlist;

import org.pz.platypus.GDD;
import org.pz.platypus.interfaces.IPlugin;

import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Mainline of fontlist plugin
 * @author alb
 */
public class Fontlist implements IPlugin
{
    private GDD gdd;
    private FontDirectoriesFile fontDirFile;
    private FontDirectoriesEntries fontDirs;
    private Collection<String> filenames;
    private TreeMap<String, List<String>> fontFamilies;
    private String fontlistName;

    public Fontlist() {}

    @Override
    public void process( GDD Gdd )
    {
        initialize( Gdd );
        create( null );
    }

    public void initialize( GDD Gdd )
    {
        gdd = checkNotNull( Gdd, "GDD passed to FontList.java is null." );
        checkNotNull( gdd.homeDirectory, "Home directory in GDD is null in FontList.java" );
    }

    public void create( final String reportName )
    {
        // 1. Get list of user-defined font directories
        if( gdd.homeDirectory != null ) {
            fontDirFile = new FontDirectoriesFile( gdd.homeDirectory + "config/fontdirs.txt", gdd );
            fontDirs = fontDirFile.extractFontDirectories();

        // 2. Add list of default font directories
            DefaultFontDirectories defaultDirs = new DefaultFontDirectories( gdd );
            defaultDirs.load( fontDirs );

        // 3. Extract list of font file names in those directories
            FontFilenamesExtractor filenameExtractor = new FontFilenamesExtractor( gdd );
            filenames = filenameExtractor.getFilenames( fontDirs );

        // 4. Look up the font family names associated with each font file
            FontFamilyNameExtractor familyNameExtractor = new FontFamilyNameExtractor( gdd );
            fontFamilies = familyNameExtractor.getFontFamilies( filenames );

        // 5. Write the file out containing the font family name and its associated font files
            if( reportName == null || reportName.isEmpty() )
                fontlistName = gdd.homeDirectory + "config/fontlist.txt";
            else
                fontlistName = reportName;

            ReportWriter report = new ReportWriter( gdd );
            report.create( fontlistName, fontFamilies );
        }
    }

    //=== for testing only ===//
    public Collection<String>            getFilenames() { return( filenames ); }
    public FontDirectoriesEntries        getFontDirectories() {  return( fontDirs );  }
    public TreeMap<String, List<String>> getFontFamilies() { return( fontFamilies ); }
}
