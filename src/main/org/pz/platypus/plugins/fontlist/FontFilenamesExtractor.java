/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.plugins.fontlist;

import org.pz.platypus.GDD;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Extracts names of font files from a list of directories
 * @author alb
 */
public class FontFilenamesExtractor
{
    GDD gdd;

    FontFilenamesExtractor( GDD Gdd )
    {
        gdd = Gdd;
    }

    /**
     * Return a Collection of font files found in the passed-in list of font directories.
     * Font files must use a supported format: .ttf, .otf, .afm, or .ttc
     *
     * @param dirs array of directory names that should contain fonts
     * @return Collection of filenames. Will have length = 0, if no font files were found.
     */
    public Collection<String> getFilenames( final FontDirectoriesEntries dirs )
    {
            LinkedList<String> fontFiles = new LinkedList<String>();
            Collection<String> dirList = dirs.getList();

            for( String dirName : dirList )
            {
                File dir = new File( dirName );
                String[] fileNames = dir.list();
                if( fileNames != null && fileNames.length != 0 ) {
                    for( String fName : fileNames )
                    {
                        if( fName.toLowerCase().endsWith(".ttf") ||
                                fName.toLowerCase().endsWith(".ttc") ||
                                fName.toLowerCase().endsWith(".otf") ||
                                fName.toLowerCase().endsWith(".afm") ||
                                fName.toLowerCase().endsWith(".pfm")) {
                            fontFiles.add( dirName + "/" + fName );
                        } // end if filename has correct extension
                    } // end for every file name
                } // end if there are fileNames
            }

//            if( gdd != null )
//                gdd.diag( "Located " + fontFiles.size() + " font files" );

            return( fontFiles );
    }
}
