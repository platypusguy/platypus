/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.plugins.fontlist;

import org.pz.platypus.GDD;

/**
 * Contains the names of all the default font directories examined by Platypus
 * @author alb
 */
public class DefaultFontDirectories
{
    GDD gdd;

    public DefaultFontDirectories( GDD Gdd )
    {
        gdd = Gdd;
    }

    public void load( FontDirectoriesEntries dirs )
    {
        // these are the font directories that iText uses by default
        dirs.addFontDir( "c:/windows/fonts" );
        dirs.addFontDir( "c:/winnt/fonts" );
        dirs.addFontDir( "d:/windows/fonts" );
        dirs.addFontDir( "d:/winnt/fonts" );
        dirs.addFontDir( "/usr/X/lib/X11/fonts/TrueType" );
        dirs.addFontDir( "/usr/openwin/lib/X11/fonts/TrueType" );
        dirs.addFontDir( "/usr/share/fonts/default/TrueType" );
        dirs.addFontDir( "/usr/X11R6/lib/X11/fonts/ttf" );
        dirs.addFontDir( "/Library/Fonts" );
        dirs.addFontDir( "/System/Library/Fonts" );

        // add the fonts in PLATYPUS_HOME (if it's been defined)
        String homeDir = gdd.homeDirectory;
        if( homeDir != null )
            dirs.addFontDir(  homeDir + "fonts" );

        // add the fonts in the JVM font directory
        String jvmDir = System.getProperty( "java.home" );
        if( jvmDir != null )
            dirs.addFontDir(  jvmDir + "/lib/fonts" );
    }
}
