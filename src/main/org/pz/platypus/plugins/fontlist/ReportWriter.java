/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.plugins.fontlist;

import org.pz.platypus.GDD;

import java.io.*;
import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Accepts the tree of family-name keys and associated font filenames as values and writes them out
 * in alpha order to the fontlist report whose name is specified in the filename parameter in create()
 * @author alb
 */
public class ReportWriter
{
    private GDD gdd;

    public ReportWriter( final GDD Gdd )
    {
        gdd = checkNotNull( Gdd, "GDD passed to fontList/ReportWriter.java is null." );
    }

    public void create( String filename, TreeMap<String, List<String >> entries )
    {
        FileWriter outFile;

        try {
            String fileName = checkNotNull( filename, "Null filename in fontlist/ReportWriter.create()" );
            outFile = new FileWriter( fileName );
            PrintWriter out = new PrintWriter( outFile );

            outputEntries( out, entries );
            outFile.close();
            gdd.diag( "Completed writing font list to: " + filename );
        }
        catch ( IOException ioe ) {
            gdd.log.severe( gdd.getLit( "ERROR.FONTLIST_IO" )+ ": " + filename );
        }
    }

    /**
     * Write out one line for each file in the family: familyname=filename\n
     * @param report the file to write to
     * @param fontFamilies the font families to write out
     */
    public void outputEntries( final PrintWriter report, TreeMap<String, List<String>> fontFamilies )
    {
        for(  Map.Entry<String, List<String >> nextFamily : fontFamilies.entrySet() ) {
            for( String filename : nextFamily.getValue() )
                report.println(nextFamily.getKey() + "=" + filename);
        }
    }
}
