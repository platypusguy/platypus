/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.plugins.fontlist;

import org.pz.platypus.utilities.TextTransforms;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import static com.google.common.base.Preconditions.*;

/**
 * Contains a list of font directories (obtained from a font directories file in FontDirectoriesFile.java)
 * @author alb
 */
public class FontDirectoriesEntries
{
    private ArrayList<String> fontDirList;

    public FontDirectoriesEntries()
    {
        fontDirList = new ArrayList<String>( 20 );
    }

    /**
     * Add a font name making sure to remove any trailing / or \
     * @param dirName  name/location of font directory to add
     */
    @SuppressWarnings("unchecked")
    protected void addFontDir( final String dirName )
    {
        String dir = checkNotNull( dirName,
                                   "font directory name passed to FontDirectoriesEntries.addFontDir() is null." );

        if( new File( dirName ).isDirectory() )
            dir = dirName.endsWith( "/" ) || dirName.endsWith( "\\" ) ?
                    TextTransforms.truncate( dirName, 1 ) : dirName;
        try {
            fontDirList.add( dir );
        }
        catch( Throwable t ) {
            // do nothing. If it doesn't work, the directory entry isn't added. That's all.
        }
    }

    public Collection<String> getList()
    {
        return( fontDirList );
    }

    public int size()
    {
        return( fontDirList.size() );
    }

    // following are used primarily (exclusively?) for unit testing.
    public String get( int position )
    {
        return( fontDirList.get( position ));
    }
}
