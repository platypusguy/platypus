/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-12 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.plugins.fontlist;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.BaseFont;
import org.pz.platypus.GDD;

import java.io.*;
import java.util.*;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Looks up the family for a font file and creates a TreeMap mapping font-family names to 1+ font files.
 * @author alb
 */
@SuppressWarnings( "unchecked" )
public class FontFamilyNameExtractor
{
    GDD gdd;

    // a sorted map consisting of a family name (such as "Garamond") and a list of all the font files that map to it.
    private TreeMap<String, List<String>> familyTree;

    public FontFamilyNameExtractor( GDD Gdd )
    {
        familyTree = new TreeMap<String, List<String>>();
        gdd = checkNotNull( Gdd );
    }

    /**
     * Uses iText to look up the family names buried in the font files that are passed to it
     * @param filenames Collection of Strings, each a filename to a font file
     * @return a tree with entries consisting the family (key) and a linked list of font files in that family (value)
     */
    public TreeMap<String, List<String>> getFontFamilies( Collection<String> filenames )
    {
        LinkedList<String[]> familyAndFont = new LinkedList<String[]>();

        String[] family;
        BaseFont bf = null;
        int i = 0;

        Collection<String> files = checkNotNull( filenames );
        for( String filename : files ) {
            family = extractFamilyNames(filename, bf );
        //    System.out.println( "Family: " + family[0] + " File: " + filename );
            String[] x = { family[0], filename };
            familyAndFont.add( x );
            i++;
        }

        gdd.diag( "Found " + i + " files for the font list" );

        String filename = "";
        try {
            for( String[] kv : familyAndFont ) {
                filename = kv[1];
                if( familyTree.containsKey( kv[0] )) {
                    List v = familyTree.get( kv[0] );
                    v.add( filename );
                }
                else {
                    List l = new LinkedList<String>();
                    l.add( filename );
                    familyTree.put( kv[0], l );
                }
            }
        } catch ( Throwable t ) {
            gdd.log.warning( "Internal exception occurred adding " + filename + " to fontlist tree." );
        }

        return( familyTree );
    }

    /**
     * Uses iText to extract the family name (or in case of .ttc the names) of the font family.
     *
     * @param fontFilename font file
     * @param bf is an iText basefont data structure from which the font info is extracted
     * @return the font's family name(s) or an empty array if an error occurred.
     *
     * For explanation of .ttc handling, see:
     * http://itextdocs.lowagie.com/tutorial/fonts/getting/index.php
     */
    public String[] extractFamilyNames( String fontFilename, BaseFont bf )
    {
        String familyName[] = new String[1];

        String names[][];
        File f = new File( fontFilename );
        if( f.exists() || ! f.isDirectory() ) {
            try {
                if( fontFilename.toLowerCase().endsWith( ".ttc")) {
                    return( BaseFont.enumerateTTCNames( fontFilename ));
                }

	            bf = BaseFont.createFont( fontFilename, "winansi", BaseFont.NOT_EMBEDDED );
                names = bf.getFamilyFontName();
	        }
            catch( IOException ioe ) {
                gdd.log.fine( "IOException loading " + fontFilename + " into font-family map" );
                return( new String[0] );
            }
            catch( DocumentException de ) {
                gdd.log.fine( "iText DocumentException loading " + fontFilename + " into font-family map" );
                return(new String[0] );
            }

            if( names!= null && names[0] != null ) {
                familyName[0] = names[0][3];
                return familyName;
            }
            else {
                return( new String[0] );
            }
        }
        return( new String[0] );
    }
}
