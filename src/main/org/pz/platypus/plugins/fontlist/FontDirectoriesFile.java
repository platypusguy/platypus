/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.plugins.fontlist;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import org.pz.platypus.GDD;

import java.io.*;
import java.util.*;

import static com.google.common.base.Preconditions.*;

/**
 * Extracts a FontDirectoreiesEntries object containg the names of all the font directories
 * int the text file, fontdirs.txt, in PLATYPUS_HOME/config that lets the user specify
 * additional directories to search for font files for inclusion in the fontlist.
 *
 * @author alb
 */
public class FontDirectoriesFile
{
    String filename;
    GDD gdd;

    public FontDirectoriesFile( final String fileName, final GDD Gdd )
    {
        checkNotNull( fileName, "filename passed to FontDirectoriesFile() is null." );
        checkArgument( ! fileName.isEmpty(), "filename passed to FontDirectoriesFile() is empty" );
        filename = fileName;

        gdd = checkNotNull( Gdd, "GDD passed to FontDirectoriesFile() is null." );
    }

    public FontDirectoriesEntries extractFontDirectories()
    {
        List<String> entries = new ArrayList<String>( 20 );
        File fontDirsFile = new File( filename );

        if( fontDirsFile.exists() && fontDirsFile.canRead() ) {
            try {
                entries = Files.readLines( fontDirsFile, Charsets.UTF_8 );
            } catch (IOException e) {
                gdd.log.warning( gdd.getLit( "ERROR.INVALID_FONT_DIRECTORY_FILE" ) + ": " +
                        filename + " " + gdd.getLit( "IGNORED" ));
            }
        }

        FontDirectoriesEntries dirList = new FontDirectoriesEntries();
        for( String entry : entries ) {
            if( ! ( entry.isEmpty() || Character.isWhitespace( entry.charAt( 0 )) || entry.startsWith( "#" ))) {
               dirList.addFontDir( entry.trim() );
            }
        }

        gdd.diag( "Font directory file loaded with " + dirList.size() + " entries: " + filename );
        return( dirList );
    }
}
