/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-12 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Checks whether there is an option in the config file for this output format that
 * says, "Don't have Platypus parse it." This would be the case for plugins that want
 * to do their own parsing.
 *
 * Further observation: Looks a lot like YAGNI. Probably can be removed if there's no clear need for it.
 *
 * @author alb
 */
public class ParseVerifier {
    private GDD gdd;
    private boolean needToParse;


    public ParseVerifier( GDD Gdd )
    {
        gdd = checkNotNull( Gdd );
    }

    public boolean isNeeded()
    {
        ConfigFile config = checkNotNull( gdd.configFile );
        String format = gdd.sysStrings.get( "_FORMAT" );

        String parseOption = config.getConfigItem("pi.out." + format + ".platyparse");
        if( parseOption != null && parseOption.toLowerCase().equals( "no" ))
            return( false );
        return( true );
    }

}
