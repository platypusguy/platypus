/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-12 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus;

import org.pz.platypus.exceptions.StopExecutionException;
import org.pz.platypus.utilities.FileSeparator;

import java.io.File;

/**
 * Locates where the output plugin file is for a given output format.
 * @author alb
 */
public class PluginLocator
{
    GDD gdd;

    String pluginFilepath = null;

    public PluginLocator( GDD Gdd )
    {
        gdd = Gdd;
    }

    // for testing only TODO: verify if this is needed.
    public PluginLocator( final String Location )
    {
        pluginFilepath = Location;
    }

    /**
     * Look up output plugin in config file. If an entry is found, make sure the file it points to exists.
     * @param formatToLocate format whose plugin is being sought
     * @return the validated location of the file; if not found, throws an exception
     * @throws  StopExecutionException if case plugin cannot be located
     */
    public String locate( final String formatToLocate )
    {
        String format = formatToLocate.toLowerCase();

        final String pluginFilename = gdd.configFile.getConfigItem( "pi.out." + format );
        if( ! pluginFilename.equals( " " )) {
            pluginFilepath = gdd.homeDirectory + "plugins" + FileSeparator.get() + pluginFilename;
            File pluginFile = new File( pluginFilepath );
            if( pluginFile.exists() )
                return( pluginFilepath );
        }

        gdd.log.severe( gdd.getLit( "ERROR.MISSING_OUTPUT_PLUGIN_FOR_FORMAT" ) + " " + format +
        ". " + gdd.getLit( "EXPECTED_IT_HERE" ) + ": " + pluginFilepath );
        throw new StopExecutionException( null );
    }

    public String getLocation()
    {
        return( pluginFilepath );
    }
}
