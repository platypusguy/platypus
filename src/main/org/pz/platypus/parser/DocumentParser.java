/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.parser;

import org.pz.platypus.GDD;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Parses input document and puts the resulting tokens in GDD.
 * @author alb
 */
public class DocumentParser
{
    private GDD gdd;

    public DocumentParser( GDD Gdd )
    {
        gdd = checkNotNull( Gdd );
    }

    public void parseFile()
    {
        String filename = checkNotNull( gdd.sysStrings.get( "_INPUT_FILE" ));
        parseFile( filename );
    }

    /**
     * Parses the file and adds the resulting tokens to docTokens in GDD
     * @param filename name of file to parse.
     */
    public void parseFile( final String filename )
    {
        TokenList tokens = new FileParser( filename, gdd ).parse();
        gdd.docTokens.addAll( tokens );
    }
}
