/***
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus.parser;

import org.pz.platypus.ConfigFile;
import org.pz.platypus.SystemStrings;
import org.pz.platypus.parser.TokenEmitters.*;
import org.pz.platypus.utilities.TextTransforms;

import static com.google.common.base.Preconditions.*;

/**
 * Emits the various tokens to the token list
 *
 * @author alb
 */
public class TokenEmitter
{
    TokenList tokenList;
    ConfigFile configFile;

    //== the various token emitters ==/
    BlankLineEmitter        blankLineGen;
    BlockCommentEmitter     blockCommentGen;
    EolEmitter              eolGen;
    EscapedCommandtEmitter  escCommand;
    LineCommentEmitter      lineCommentGen;
    TextEmitter             textGen;

    public TokenEmitter( TokenList tokens, ConfigFile config, SystemStrings settings )
    {
        tokenList = checkNotNull( tokens );
        configFile = checkNotNull( config );

        blankLineGen = new BlankLineEmitter( tokens, config, settings );
        blockCommentGen = new BlockCommentEmitter( tokens, config, settings );
        eolGen = new EolEmitter( tokens, config, settings );
        escCommand = new EscapedCommandtEmitter( tokens, config, settings );
        textGen = new TextEmitter( tokens );
    }

    public void blankLine( final Source source, final ParserState state )
    {
        blankLineGen.emit( source, state );
    }

    public void blockComment( final Source source, final String commentText )
    {
        blockCommentGen.emit( source, commentText );
    }

    public void eol( final Source source, final ParserState state )
    {
        eolGen.emit( source, state );
    }

    public void escapedCommand( final Source source )
    {
        escCommand.emit( source );
    }

    public void lineComment( final Source source, final String commentText )
    {
        lineCommentGen.emit( source, commentText );
    }
    public void text( final Source source, final String text )
    {
        textGen.emit( source, text );
    }

    public void text( final Source source, int start, int end, char[] textLine )
    {
        checkArgument( start >= 0 && start < end );
        checkArgument( end > 0 );

        final String tokenText = TextTransforms.charArrayToString( textLine, start, end );
        textGen.emit( source, tokenText );
    }
}