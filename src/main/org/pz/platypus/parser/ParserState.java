/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.parser;

/**
 * Contains the parser state fields that can span more than one line: in comment, in script, in code, etc.
 * Allows the parser to parse via repeated calls to a single-line parsing routine.
 *
 * @author alb
 */
public class ParserState
{
    /** if we're in a comment block, this is the closing symbol to look for. Null, if we're not. */
    String closingCommentBlock;

    /** in a code block */
    private boolean inCode;

    /** in a script */
    private boolean inScript;

    public ParserState()
    {
        this( null, false, false );
    }

    public ParserState( final String closingComment, final boolean code, final boolean script )
    {
        closingCommentBlock = closingComment;
        inCode = code;
        inScript = script;
    }

    public boolean inCommentBlock() { return( closingCommentBlock != null ); }
    public boolean inCode() { return inCode; }
    public boolean inScript() { return inScript; }
    public void setInScript( boolean setting ) { inScript = setting; }
}
