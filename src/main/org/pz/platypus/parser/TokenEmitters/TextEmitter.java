/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.parser.TokenEmitters;

import org.pz.platypus.ConfigFile;
import org.pz.platypus.SystemStrings;
import org.pz.platypus.parser.*;

/**
 * Emits text token.
 * Note: no null checks are made, b/c they're all done by the calling routines.
 *
 * @author alb
 */
public class TextEmitter
{
    private TokenList tokenList;

    public TextEmitter( TokenList tokens )
    {
        tokenList = tokens;
    }

    /**
     * Emits the actual token to the token list if it's not in a special section of the input file (script, etc.)
     * @param source location in the file, needed for all tokens
     * @param text the text to output
     */
    public void emit( final Source source, String text )
    {
        tokenList.add( new Token( source, TokenType.TEXT, text ));
    }
}
