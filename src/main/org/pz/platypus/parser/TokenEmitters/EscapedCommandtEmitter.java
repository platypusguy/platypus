/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.parser.TokenEmitters;

import org.pz.platypus.ConfigFile;
import org.pz.platypus.SystemStrings;
import org.pz.platypus.parser.*;

/**
 * Emits an escaped command as text, either /[ or [, depending on config file.
 * Note: no null checks are made, b/c they're all done by the calling routines.
 *
 * @author alb
 */
public class EscapedCommandtEmitter
{
    private TokenList tokenList;
    private ConfigFile configFile;
    private String outputFormat;

    public EscapedCommandtEmitter(TokenList tokens, ConfigFile config, SystemStrings settings)
    {
        tokenList = tokens;
        configFile = config;
        outputFormat = settings.get( "_FORMAT" );
    }

    /**
     * Emits the actual token to the token list if tokens are actually emitted
     * @param source location in the file, needed for all tokens
     */
    public void emit( final Source source )
    {
        TextEmitter textEmit = new TextEmitter( tokenList );
        if( includeEscapeCharsInTokenStream() )
            textEmit.emit( source, "/[" );
        else
            textEmit.emit( source, "[" );
    }

    /**
     * Are escaped commands passed through to the output file?
     * @return true, if escapec commands are passed through, false otherwise.
     */
    private boolean includeEscapeCharsInTokenStream()
    {
        String keepComments =
                configFile.getConfigItem( "pi.out." + outputFormat + ".passthrough_escape_char" );
        return ( keepComments.toLowerCase().equals( "yes" ));
    }
}
