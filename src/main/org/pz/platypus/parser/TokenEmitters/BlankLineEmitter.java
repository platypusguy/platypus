/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.parser.TokenEmitters;

import org.pz.platypus.ConfigFile;
import org.pz.platypus.SystemStrings;
import org.pz.platypus.parser.*;

/**
 * Emits a blank line token.
 * Note: no null checks are made, b/c they're all done by the calling routines.
 *
 * @author alb
 */
public class BlankLineEmitter
{
    private TokenList tokenList;
    private ConfigFile configFile;
    private String outputFormat;

    public BlankLineEmitter( TokenList tokens, ConfigFile config, SystemStrings settings )
    {
        tokenList = tokens;
        configFile = config;
        outputFormat = settings.get( "_FORMAT" );
    }

    /**
     * Emits the actual token to the token list if it's not in a special section of the input file (script, etc.)
     * @param source location in the file, needed for all tokens
     * @param state identifies whether the parser is in a special section of the input file
     */
    public void emit( final Source source, final ParserState state )
    {
        if( state.inCommentBlock() ) { // don't emit blank lines in comment blocks, if comments aren't passed thru
            if( ! includeCommentsInTokenStream() )
                return;
        }

        if( state.inScript() )  // don't emit blank line commands in scripts.
            return;

        tokenList.add( new Token( source, TokenType.COMMAND, "[CR]", "[CR]", null ));
    }

    /**
     * Are comments passed through to the output file?
     * @return true, if comments are passed through, false otherwise.
     */
    private boolean includeCommentsInTokenStream()
    {
        String keepComments =
                configFile.getConfigItem( "pi.out." + outputFormat + ".keep_comments" );
        return ( keepComments.toLowerCase().equals( "yes" ));
    }
}
