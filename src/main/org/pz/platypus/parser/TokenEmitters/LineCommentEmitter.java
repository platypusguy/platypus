/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.parser.TokenEmitters;

import org.pz.platypus.ConfigFile;
import org.pz.platypus.SystemStrings;
import org.pz.platypus.parser.Source;
import org.pz.platypus.parser.Token;
import org.pz.platypus.parser.TokenList;
import org.pz.platypus.parser.TokenType;

/**
 * Emits a line comment token.
 * Note: no null checks are made, b/c they're all done by the calling routines.
 *
 * @author alb
 */
public class LineCommentEmitter
{
    private TokenList tokenList;
    private ConfigFile configFile;
    private String outputFormat;

    public LineCommentEmitter(TokenList tokens, ConfigFile config, SystemStrings settings)
    {
        tokenList = tokens;
        configFile = config;
        outputFormat = settings.get( "_FORMAT" );
    }

    /**
     * Emits the actual token to the token list if tokens are actually emitted
     * @param source location in the file, needed for all tokens
     * @param comment the contents of the comment
     */
    public void emit( final Source source, String comment )
    {
        if( includeCommentsInTokenStream() )
            tokenList.add( new Token( source, TokenType.LINE_COMMENT, comment, null, null ));
    }

    /**
     * Are comments passed through to the output file?
     * @return true, if comments are passed through, false otherwise.
     */
    private boolean includeCommentsInTokenStream()
    {
        String keepComments =
                configFile.getConfigItem( "pi.out." + outputFormat + ".keep_comments" );
        return ( keepComments.toLowerCase().equals( "yes" ));
    }
}
