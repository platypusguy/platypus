/**
 * Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 * Platypus is (c) Copyright 2009-16 Andrew Binstock. All Rights Reserved.
 * Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus;

import org.pz.platypus.commandline.*;
import org.pz.platypus.exceptions.StopExecutionException;
import org.pz.platypus.utilities.PlatypusHomeDirectory;
import org.pz.platypus.parser.*;

/**
 * The main line. Called by Platypus.java
 * @author alb
 */

public class Main
{
    ClParser commandLine;
    GDD gdd;

    public void run( String[] args )
    {
        displayCopyright();
        try {
            // set up data structures: GDD, lits.
            PlatypusHomeDirectory homeDir = new PlatypusHomeDirectory( args );
            gdd = new GDD( homeDir );
            Literals lits = new Literals( gdd );
            gdd.setupGdd( lits );

            // initial processing of command line
            ClProcessor clproc = new ClProcessor( args, gdd );
            clproc.processVerbosityOptions();

            // load config file and load command file into command table
            gdd.configFile = new ConfigFile( new ConfigLocator( homeDir ).get(), gdd );
            gdd.commandTable.load();

            // main command-line processing
            String pluginLocation = clproc.process();

            // if there's an output plugin to run, then...
            if( pluginLocation != null ) {
                // parse the input file
                if( new ParseVerifier( gdd ).isNeeded() ) {
                    new DocumentParser( gdd ).parseFile();
                }

                // now run the output plugin
                PluginLoader pluginLoader = new PluginLoader( pluginLocation, gdd );
                pluginLoader.runPlugin();
            }
        } catch( StopExecutionException see ) {
            // Do nothing, as all error messages have already been shown to the user.
        }

        // Show any token lists, if -vverbose is specified
        if( gdd.clVVerbose )
            gdd.docTokens.dump( gdd );
    }

    /**
     * Display the copyright notice on the console.
     */
    private void displayCopyright( )
    {
        CopyrightNotice copyright = new CopyrightNotice();
        // Note: v. 0.4.0 is the 0.2.7 codebase refactored and extended.
        System.out.println( "Platypus v. 0.4.0 " + copyright.toString() );
    }
}
