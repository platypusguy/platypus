/**
 * Platypus: Page Layout and Typesetting Software (free at platypus.pz.org.org)
 *
 * Platypus is (c) Copyright 2011 Pacific Data Works LLC. All Rights Reserved.
 * Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus;

/**
 * Where execution begins.
 *
 * @author alb
 */
public class Platypus
{
    /**
     * Everything starts here...
     *
     * @param args command-line args
     */
    public static void main( final String[] args )
    {
        try {
            Main mainLine = new Main();
            mainLine.run( args );
        } catch (Throwable t ) {
            System.out.println( "An unexpected error occurred. Please contact feedback@pz.org." );
        }
    }
}

