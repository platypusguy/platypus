/***
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus.exceptions;

import org.pz.platypus.GDD;
import org.pz.platypus.parser.Source;

import static com.google.common.base.Preconditions.*;

/**
 * Exception used for many types of unexpected, internal errors that the user won't likely understand.
 * Generally suggest contacting the Platypus team about the problem.
 *
 * @author based on code originally from atul s. khot; modified by alb
 */
public class InternalException extends PlatyException
{
    GDD gdd;

    public InternalException( GDD Gdd, Source source, String errorLit, String className )
    {
        super( checkNotNull( errorLit), className );
        gdd = checkNotNull( Gdd );
        checkNotNull( source );

        gdd.log.severe( gdd.getLit( "ERROR.UNEXPECTED" ) + "\n" +
                        gdd.getLit( "FILE#" ) + " " + source.getFileNumber() + " " +
                        gdd.getLit( "LINE#" ) + " " + source.getLineNumber() + " " +
                        gdd.getLit( errorLit ) +  gdd.getLit( "IN" ) + className );
        throw new StopExecutionException( null );
    }

    public InternalException() { super( null ); }
}
