/***
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus.exceptions;

/**
 * Exception raised for an operation that can only be performed in the plugin. For example,
 * converting units of lines to points: Only the plugin knows what the the existing size in
 * points of a line's leading is.
 *
 * @author alb
 */
public class PluginOnlyOperationException extends PlatyException
{
    public PluginOnlyOperationException( final String errMsg, final String filename )
    {
        super( errMsg, filename );
    }
}
