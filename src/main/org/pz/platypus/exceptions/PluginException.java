/***
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2011 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus.exceptions;

/**
 * Exception used for stopping exception after an error occurs in a running plugin
 *
 * @author based on code originally from atul s. khot; modified by alb
 */
public class PluginException extends PlatyException
{
    public PluginException( String errMsg )
    {
        super(errMsg);
    }
}
