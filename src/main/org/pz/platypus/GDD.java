/***
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus;

import org.pz.platypus.parser.TokenList;
import org.pz.platypus.utilities.FileSeparator;
import org.pz.platypus.logging.*;
import org.pz.platypus.utilities.PlatypusHomeDirectory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.*;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Global document data. Is a singleton, although not created as such one due to testing repercussions.
 *
 * @author alb
 * @author atul khot
 */
public class GDD
{
    public boolean          clVerbose = false;      // command line -verbose switch
    public boolean          clVVerbose = false;     // command line -vverbose switch
    public CommandTable     commandTable;
    public ConfigFile       configFile;
    public String           fileSeparator;          // fileSeparator char for user platform
////    private Stack<Infile>   filesToProcess;
    public String           homeDirectory;
//    private FileList        inputFileList;
//    private LineList        inputLines;
    public TokenList        docTokens;
    public Literals         lits;
    public LogConsole       log;
//    private String          outputPluginPrefix;
    public SystemStrings   sysStrings;
//    private Map<String,String> userEnv;
//    private UserStrings     userStrings;
//
    public GDD()
    {
        initialize();
    }

    public GDD( String homeDir )
    {
        homeDirectory = checkNotNull( homeDir );
        initialize();
    }

    public GDD( PlatypusHomeDirectory phd )
    {
        homeDirectory = checkNotNull( phd ).get();
        initialize();
    }

    /**
     * initialize many of the fields in GDD. To completely initialize GDD, the following
     * calls are must be made (cf. Platypus.java.setupGdd()):
     */
    public void initialize()
    {
        sysStrings = new SystemStrings();
        loadDatetoSysStrings();
        commandTable = null;
        fileSeparator = FileSeparator.get();
//        inCode = false;
//        inputFileList = new FileList();
//        inputLines = new LineList();
        docTokens = new TokenList();
//        userEnv = System.getenv();
//        userStrings = new UserStrings();
//        loadRgbColorsIntoUserStrings( userStrings );
    }

    public void setupGdd( final  Literals Lits )
    {
        lits = checkNotNull( Lits );
        log = new LogConsole( null, lits );  // should run after initialization of lits
        commandTable = new CommandTable( this );
    }
//
//    /**
//     * Loads the 16 HTML standard colors as RGB values into the user strings map
//     * @param userMacros the KV table containing user-defined macros
//     */
//    public void loadRgbColorsIntoUserStrings( final UserStrings userMacros )
//    {
//        userMacros.add( "AQUA",    "0,255,255" );    // x00FFFF
//        userMacros.add( "BLACK",   "0,0,0" );        // x000000
//        userMacros.add( "BLUE",    "0,0,255" );      // x0000FF
//        userMacros.add( "FUSCHIA", "255,0,255" );    // xFF00FF
//        userMacros.add( "GRAY",    "128,128,128" );  // x808080
//        userMacros.add( "GREY",    "128,128,128" );  // x808080 --- same as GRAY
//        userMacros.add( "GREEN",   "0,128,0" );      // x008000
//        userMacros.add( "LIME",    "0,255,0" );      // x00FF00
//        userMacros.add( "MAROON",  "128,0,0" );      // x800000
//        userMacros.add( "NAVY",    "0,0,128" );      // x000080
//        userMacros.add( "OLIVE",   "128,128,0" );    // x808000
//        userMacros.add( "PURPLE",  "128,0,128" );    // x800080
//        userMacros.add( "RED",     "255,0,0" );      // xFF0000
//        userMacros.add( "SILVER",  "192,192,192" );  // xC0C0C0
//        userMacros.add( "TEAL",    "0,128,128" );    // x008080
//        userMacros.add( "WHITE",   "255,255,255" );  // xFFFFFF
//        userMacros.add( "YELLOW",  "255,255,0" );    // xFFFF00
//    }
//


    /**
     * Diagnostic info (not part of the logger). Writes to stdout on -verbose or higher setting.
     * Does not use Literals file, as we want all diagnostic data in English for our use.
     * Also adds the date and time to the log message sent by the calling method.
     *
     * @param msg the message to log to the console
     */
    public void diag( final String  msg )
    {
        if( clVerbose ) {
            String s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new java.util.Date());
            System.out.println( s + " " + msg );
        }
    }

    /**
     * Loads the date into the system table
     */
    public void loadDatetoSysStrings()
    {
        Calendar rightNow = Calendar.getInstance();
        sysStrings.add( "_datedd", String.format( "%02d", rightNow.get( Calendar.DAY_OF_MONTH )));
        sysStrings.add( "_datemm", String.format( "%02d", rightNow.get( Calendar.MONTH )));
        sysStrings.add( "_dateyyyy", Integer.toString( rightNow.get( Calendar.YEAR )));
    }

    public String getLit( final String key )
    {
        return( lits.getLit( key ));
    }

    // used for testing only.
    public void setLits( final Literals newLits )
    {
        lits = newLits;
    }

    public void setClVerbose( boolean clVerbose )
    {
        this.clVerbose = clVerbose;
        this.log.setLevel( clVerbose ? Level.FINE : Level.WARNING );
    }

    /**
     * -vverbose implies -verbose, so both are set, if true. Warning level, if turned off.
     * @param beVerbose  true/false setting
     */
    public void setClVVerbose( boolean beVerbose )
    {
        setClVerbose( beVerbose );
        this.clVVerbose = beVerbose;
        this.log.setLevel( clVerbose ? Level.FINEST : Level.WARNING );
    }
}
