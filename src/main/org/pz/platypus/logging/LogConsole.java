/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.logging;

import org.pz.platypus.Literals;

import java.util.logging.*;

/**
 * @author alb
 */
public class LogConsole
{
    public Logger logger;

    public LogConsole()
    {
        this( null, null );
    }

    /**
     * set up the logger we'll use and give it our own formatting.
     * This logger writes to the console only.
     *
     * @param loggerName name for the logger
     * @param lits where literals are. Used in setting up the LogFormatter.
     */
    public LogConsole( final  String loggerName, final Literals lits )
    {
        logger = Logger.getLogger( loggerName == null ? "org.pz.platypus.Platypus" : loggerName );

        ConsoleHandler consh = new ConsoleHandler();
        consh.setLevel( Level.ALL );
        consh.setFormatter( new LogFormatter( lits ));
        logger.addHandler( consh );
        logger.setUseParentHandlers( false );

        // By default we log only errors and warnings. To get info-level messages, the
        // user must specify -verbose on command line. To get all info (mostly for debug),
        // user must specify -vverborse on command line (that is, very verbose)
        logger.setLevel( Level.WARNING );
    }

    // user-friendly names for various functions of the Logger
    public void severe( final String msg )  { logger.severe( msg ); }
    public void warning( final String msg ) { logger.warning( msg ); }
    public void info( final String msg )    { logger.info( msg ); }
    public void fine( final String msg )    { logger.fine( msg ); }
    public void finer( final String msg )   { logger.finer( msg ); }
    public void finest( final String msg )  { logger.finest( msg ); }

    public void setLevel( Level l )         { logger.setLevel( l ); }
    public Level getLevel()                 { return( logger.getLevel() ); }     // used for testing
}
