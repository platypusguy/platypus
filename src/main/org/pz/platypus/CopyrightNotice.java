/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Andrew Binstock. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus;

import org.pz.platypus.utilities.DateTime;

/**
 * Assembles the literal for the copyright notice that appears on the console
 * every time that Platypus is run.
 *
 * @author alb
 */
public class CopyrightNotice
{
    private String copyrightDate;

    public CopyrightNotice()
    {
        copyrightDate = computeCopyrightDateRange();
    }

    /**
     * Get the copyright date range as 2009 to the present year.
     * @return a string showing the range of copyright dates.
     */
    protected String computeCopyrightDateRange()
    {
        int currYear = new DateTime().getYear();

        String copyrightDate = "2009";
        if( currYear > 2009 ) {
            currYear -= 2000;
            copyrightDate += ( "-" +  currYear );
        }
        return( copyrightDate );
    }

    @Override
    public String toString()
    {
        return( "© " + copyrightDate + " " +
                "by Andrew Binstock. All rights reserved." );
    }
}
