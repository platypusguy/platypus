/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2011 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.commandline;

/**
 *  The options that are simply specified or not on the command line. Such as -help.
 *  They take no parameters. The term is borrowed from the Apache CLI package
 *
 * @author alb
 */
public class BooleanOption
{
    private String name;
    private String description;

    public BooleanOption( final String name, final String desc )
    {
        this.name = name;
        this.description = desc;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }
}
