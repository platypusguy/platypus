/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.commandline;

/**
 * An array of all Platypus boolean options.
 *
 * @author alb
 */
public class BooleanOptions
{
    /** list of all boolean options supported in Platypus */
    private BooleanOption[] booleanOptions;

    /**
     *  To add new boolean options to Platypus, do so here.
     */
    public BooleanOptions()
    {
        booleanOptions = new BooleanOption[] {
            new BooleanOption( "verbose",  "verbose processing" ),
            new BooleanOption( "vverbose", "very verbose processing" ),
            new BooleanOption( "fontlist", "list of fonts" ),
            new BooleanOption( "help",     "print this message" )
        };
    }

    public int getLength()
    {
        return( booleanOptions.length );
    }

    public BooleanOption[] getOptions()
    {
        return( booleanOptions );
    }
}
