/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.commandline;

/**
 * An array of all Platypus options that take arguments.
 *
 * @author alb
 */
public class ArgumentOptions
{
    /** list of all argument options supported by Platypus. */
    private ArgumentOption[] argumentOptions;

    /**
     *  To add new argument options to Platypus, do so here.
     */
    public ArgumentOptions()
    {
        argumentOptions = new ArgumentOption[] {
            new ArgumentOption( "filename", 	"input file",     	"infile" ),
            new ArgumentOption( "filename", 	"output file",    	"outfile" ),
            new ArgumentOption( "dir",	        "home directory",	"home" ),
            new ArgumentOption( "format",   	"output format",	"format" )
        };
    }

    public int getLength()
    {
        return( argumentOptions.length );
    }

    public ArgumentOption[] getOptions()
    {
        return( argumentOptions );
    }
}
