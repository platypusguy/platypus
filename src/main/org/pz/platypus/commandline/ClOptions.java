/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2011 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus.commandline;

import org.apache.commons.cli.*;
import java.util.ArrayList;

/**
 * The options for the various command-line switches. Relies on Apache Commons CLI library.
 * See commons.apache.org/cli/usage.html for an explanation of what is being done here.
 * @author alb  (based on work by Atul S. Khot, 2010)
 */
public class ClOptions
{
    /** an array of all the CLI options that Platypus supports */
    private Option[] options = new Option[8];

    private BooleanOptions  booleanOptions;
    private ArgumentOptions argumentOptions;

    /**
     * Set up all possible Platypus CL options
     */
    public ClOptions()
    {
        ArrayList<Option> cliOptions = new ArrayList<Option>();

        booleanOptions = new BooleanOptions();
        loadBooleanOptions( cliOptions, booleanOptions );

        argumentOptions = new ArgumentOptions();
        loadArgumentOptions( cliOptions, argumentOptions );

        options = cliOptions.toArray( options );
    }

    /**
     *  Add boolean options to the list of all options
     *  @param optionArray the list of all options
     *  @param booleans the boolean options to add
     */
    protected void loadBooleanOptions( final ArrayList<Option> optionArray, BooleanOptions booleans )
    {
        BooleanOption bo;

        for( int i = 0; i < booleans.getLength(); i++  ) {
            bo = booleans.getOptions()[i];
            optionArray.add( new Option( bo.getName(), bo.getDescription() ));
        }
    }

    /**
     *  Add argument options to the list of all arguments
     *  @param optionArray the list of all arguments
     *  @param argOptions the argument options to add
     */
    @SuppressWarnings( "static-access" )
    protected void loadArgumentOptions( final ArrayList<Option> optionArray, 
    									ArgumentOptions argOptions )
    {
        ArgumentOption ao;

        for( int i = 0; i < argOptions.getLength(); i++  ) {
            ao = argOptions.getOptions()[i];
            optionArray.add( OptionBuilder.withArgName( ao.getName() ).hasArg().
                             withDescription( ao.getDescription() ).create( ao.getSyntax() ));
        }
    }

    public Option[] getOptions()
    {
        return( options );
    }

    public BooleanOptions getBooleanOptions()
    {
        return( booleanOptions );
    }
}
