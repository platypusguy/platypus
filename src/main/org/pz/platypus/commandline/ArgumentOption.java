/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2011 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.commandline;

/**
 *  The options that take an argument such as -format pdf
 *  The term is borrowed from the Apache CLI package
 *
 * @author alb
 */
public class ArgumentOption
{
    private String name;            // this is the argument descriptor for the user (i.e., filename )
    private String description;     // the description as presented to the user (i.e., name of input file)
    private String syntax;          // the actual switch (w/out the leading - or --); such as: infile

    public ArgumentOption( final String name, final String desc, final String syntax )
    {
        this.name = name;
        this.description = desc;
        this.syntax = syntax;
    }

    public String getDescription()
    {
        return description;
    }

    public String getName()
    {
        return name;
    }

    public String getSyntax()
    {
        return( syntax );
    }
}
