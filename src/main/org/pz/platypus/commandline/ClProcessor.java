/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-16 Andrew Binstock. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus.commandline;

import org.apache.commons.cli.*;
import org.jetbrains.annotations.Nullable;
import org.pz.platypus.*;
import org.pz.platypus.utilities.*;

import java.util.Arrays;

/**
 * Process the command-line options.
 * @author alb; based in part on work by Atul S. Khot (ask)
 */
public class ClProcessor
{
    private GDD gdd;
    private ClParser clp;
    private String[] args;
    private String outputFormat = "";   // note empty string

    public ClProcessor( final String[] Args, final GDD Gdd )
    {
        gdd = Gdd;
        args = Args;
        clp = new ClParser( args );
    }

    @Nullable
    public String  process()
    {
        String outputLocation = null;

        CommandLine cl = clp.getLine();
        processBooleanOptions( cl );

        if( outputFormat != null ) {
            FormatSelection fs = new FormatSelection( clp, gdd );
            fs.determine();

            String format = gdd.sysStrings.get("_FORMAT");
            outputLocation = verifyOutputPlugin( format );

            updateFilenamesInSystemStrings( cl, gdd.sysStrings);
            addCommandLineToSystemStrings( args, gdd.sysStrings );
            gdd.diag( "Command line: " + gdd.sysStrings.get("_COMMAND_LINE"));
        }
        return( outputLocation );
    }

    public void processVerbosityOptions()
    {
        if ( clp.getLine().hasOption( "-vverbose" ))
            gdd.setClVVerbose( true );

        if ( clp.getLine().hasOption( "-verbose" ))
            gdd.setClVerbose( true );
    }

    public void processBooleanOptions( final CommandLine cl )
    {
        if( args.length == 0 )
            gdd.log.severe( gdd.getLit( "PLEASE_RERUN_WITH_FILENAMES" ));  // falls through to next test.

        processVerbosityOptions();

        if( cl.hasOption( "-help" ) || cl.getOptions().length == 0 ) {
            MessagesToUser.showUsage( gdd.lits, clp );
            outputFormat = null;    // indicates no further output to be done.
        }

        if ( cl.hasOption( "-fontlist" )) {
            outputFormat = "fontlist";
        }
    }

    private String verifyOutputPlugin( String format )
    {
        PluginLocator locator = new PluginLocator( gdd );
        return ( locator.locate( format ));
    }

    private void updateFilenamesInSystemStrings( final CommandLine cl, final  SystemStrings sys )
    {
        sys.add( "_INPUT_FILE", cl.getOptionValue( "infile" ));
        if( sys.get("_OUTPUT_FILE") == null ) // true except if fs.determine() computed output filename
            sys.add( "_OUTPUT_FILE", cl.getOptionValue( "outfile" ));
    }

    /**
     * Rebuilds the command-line (normalizing spacing) and adds it to the system strings table.
     * Note: Package visibility for testing only. Could otherwise be private.
     * @param args array of command-line args
     * @param sys the system table to update with the command line.
     */
    void addCommandLineToSystemStrings( final String[] args, final SystemStrings sys )
    {
        StringBuilder commandLine = new StringBuilder( 60 );
        Arrays.stream( args ).forEach( arg->
                { if( arg.contains( " " ))
                      commandLine.append( "\"" ).append( arg ).append( "\" " );
                  else
                      commandLine.append( arg ).append( " " ); });

        sys.add( "_COMMAND_LINE", commandLine.toString().trim() );
    }
}
