/***
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2006-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus;

import java.util.MissingResourceException;

/**
 * Handles getting configuration from property file. The default property file, called
 * Config.properties, must be present or the program aborts. It is in US English.
 *
 * @author alb
 */
public class ConfigFile extends PropertyFile
{
    /**
     * This constructor is included here only because it facilitates writing
     * mock ConfigFile class for testing. It's not called from Platypus working code.
     */
    public ConfigFile() { super(); }

    /**
     * Open the property file: Config.properties. If it cannot be found,
     * the program shuts down.
     * @throws java.util.MissingResourceException if resource is not found.
     * @param location identifies location of Config.properties file
     * @param gdd GDD, used for access to literals.
     */
    public ConfigFile( final String location, final GDD gdd ) throws MissingResourceException
    {
        super( location );
        try {
        	load();
        }
        catch( Exception ex ) {
            gdd.log.severe( gdd.getLit( "ERROR.MISSING_CONFIG_FILE" ) + " " +
                            gdd.getLit( "FIX.MISSING_CONFIG_FILE" ));
            throw new MissingResourceException( null, null, null );
        }

        gdd.diag( "Config file: " + location + " loaded with: " + this.getSize() + " entries." );
    }

    /**
     * Looks up a config entry in the property file. In the event of error,
     * it returns an empty string.
     *
     * @param key the name of the config entry to be looked up
     * @return the configuration data item searched for, or the empty string in case of error.
     */
    public String getConfigItem( final String key )
    {
    	return( key == null ? "" : lookup( key ));
    }

    public void testOnlySet( final String key, final String value )
    {
        testOnlyAdd( key, value );
    }
}
