/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus;

import org.pz.platypus.utilities.FileSeparator;
import org.pz.platypus.utilities.PlatypusHomeDirectory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Accepts home directory and adds the location of the configuration file in that directory.
 * @author alb
 */
public class ConfigLocator
{
    private String location;

    /**
     * Compute the location of the config file based on the home directory. This is the most common use case.
     * In testing, we might pass the location of some other .properties file, in which case we don't want any
     * modification to the location to occur.
     *
     * @param homeDirectory contains location of home directory. Or for testing, location of the specific file
     */
    public ConfigLocator( String homeDirectory )
    {
        location = formatLocation( checkNotNull( homeDirectory ) );
    }

    public ConfigLocator( PlatypusHomeDirectory phd )
    {
        location = formatLocation( checkNotNull( phd ).get() );
    }

    private String formatLocation( String loc ) {
        if( ! loc.endsWith( ".properties")) {
            return( loc += "config" + FileSeparator.get() + "Config.properties" );
        }

        return( loc );
    }

    public String get()
    {
        return( location );
    }
}
