/***
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2006-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus;

import org.pz.platypus.utilities.FileSeparator;

import java.util.MissingResourceException;

/**
 * Handles getting literal strings from property file. The default property file, called
 * Platypus.properties, must be present or the program aborts. It is in US English. If another
 * language is used, please see documentation to access the appropriate language file.
 *
 * @author alb
 */
public class Literals extends PropertyFile
{
    static String location;

    /**
     * This constructor is included here only because it facilitates writing
     * mock Literals class for testing. It's not called from Platypus working code.
     */
    public Literals() { super(); }

    /**
     * Open the property file: Literals.properties. If it cannot be found,
     * the program shuts down. Clearly, this should indeed be fatal.
     * @param gdd the GDD
     * @throws java.util.MissingResourceException if resource is not found.
     */
    public Literals( GDD gdd ) throws MissingResourceException
    {
        super( location = (  gdd.homeDirectory + "config" + FileSeparator.get() +  "Literals.properties" ));

        try {
        	load();
        }
        catch( Exception ex ) {
            // Because Literals is created and loaded before a logger has been set up,
            // errors in finding the Literals file need to be written to stderr.
            System.err.println(
            		"Missing file: " +
            		gdd.homeDirectory + "config" + FileSeparator.get() + "Literals.properties" + " " +
            		"which is required.\n" +
                    "Place it as explained in documentation and restart Platypus.\n" );
            throw new MissingResourceException( null, null, null );
        }

        gdd.diag( "Config file: " + location + " loaded with: " + this.getSize() + " entries." );
    }

    /**
     * Looks up a literal in the property file. In the event of error,
     * it returns a string consisting of a single blank.
     *
     * @param key the name of the literal to be looked up
     * @return the literal String that was searched for
     */
    public String getLit( final String key )
    {
    	return( key == null ? " " : lookup( key ));
    }
}
