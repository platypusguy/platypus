/***
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2006-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus;

import org.pz.platypus.exceptions.*;

import java.io.*;
import java.util.*;

/**
 * Reads a properties file (key-value pairs in ISO-8859 text) and parses it.
 * @author alb
 */
public class PropertyFile
{
    private String filename;

    private Properties props;

    public PropertyFile( final String propertyFilename ) {
        filename = propertyFilename;
        props = new Properties();
    }

    public PropertyFile() {	// only used when extending Property file for testing purposes.
    	this( null );
    }

    /**
     * read the file into a map
     *
     * @throws MissingResourceException if file cannot be found
     */
    public void load() {
    	InputStream is;

    	try {
    		File f = new File( filename );
    		is = new FileInputStream( f );
    	}
    	catch( Exception ex ) {
    		throw new MissingResourceException( null, null, null );
    	}

    	try {
    		props.load( is );
    	}
    	catch( Exception ex ) {
    		throw new PropertyFileException( "ERROR_LOADING_PROPERTY_FILE", filename );
        }
    }

    /**
     * Look up a property based on a search key
     * @param key the key used to look up the property
     * @return the property if found; otherwise, a space character
     */
    public String lookup( final String key )
    {
    	return( props.getProperty( key, " " ));
    }

    public int getSize() {
		return( props.size() );
	}

    public Enumeration<Object> getKeys()
    {
        return( props.keys() );
    }

    public void testOnlyAdd( final String key, final String value )
    {
        props.setProperty( key, value );
    }
}
