/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus.plugin.listing;

import com.google.common.io.Files;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.pz.platypus.Main;
import org.pz.platypus.Platypus;

import java.io.*;
import java.nio.charset.Charset;

//import com.google.common.io.Files.*;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Test whether the listing plugin works correction
 * @author alb
 */
public class ListingOutput
{
    private String infilename = "veryShort.pp";
    private String outfilename = "veryShort.html";

    private String currentDir;
    private PrintStream originalStdout = System.out;

    @Before
    public void setup()
    {
        // record where we are for possible debugging use
        File currDir = new File (".");
        currentDir = currDir.getAbsolutePath();

        // create the input file
        String contents = "Hello,[+i]world![-i][]\n";
        try {
            FileWriter writer = new FileWriter( infilename );
            writer.write( contents );
            writer.close();
        }
        catch( Throwable t ) {
            fail( "Error creating input file in test in ListingOutput.java" );
        }
    }

    @Test
    public void testOnShortFile1()
    {
        // capture stdout
        OutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream( os );
        System.setOut( ps );

        // create a listing file from the input file
        String[] args = {  infilename, outfilename, "-format", "listing" };
        Platypus platy = new Platypus();
        platy.main( args );

        // make sure it ran
        String output = os.toString();
        assertTrue( output.startsWith("Platypus v. "));

        // test contents of file
        String listing = null;
        try {
            listing = Files.toString( new File( outfilename ), Charset.defaultCharset() );
        }
        catch( IOException ioe ) {
            fail( "Could not read listing test file in ListingOuput.java" );
        }

        assertTrue( listing.contains( "Hello," ));
    }

    @After
    public void teardown()
    {
        // delete the input file and any output file
        File f = new File( infilename );
        f.delete();

        f = new File( outfilename );
        f.delete();

        // restore stdout
        System.setOut( originalStdout );
    }
}
