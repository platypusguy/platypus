/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2012 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */

package org.pz.platypus.plugins.fontlist;

import com.google.common.io.Files;
import org.junit.*;
import static org.junit.Assert.*;

import org.pz.platypus.GDD;
import org.pz.platypus.Literals;
import org.pz.platypus.mocks.MockLiterals;
import org.pz.platypus.utilities.PlatypusHomeDirectory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Test the mainline of the fontlist plugin. Slow test, so run as UAT.
 * @author alb
 */
public class FontListTest
{
    Fontlist fl;

    /** the GDD. Used to obtain the PLATYPUS_HOME directory */
    GDD gdd;

    @Before
    public void setUp()
    {
        PlatypusHomeDirectory homeDir = new PlatypusHomeDirectory( null );
        gdd = new GDD( homeDir.get() );

        Literals lits = new MockLiterals();
        gdd.setupGdd(lits);
    }

    @Test(expected = NullPointerException.class )
    public void nullHomeDirectoryShouldGiveException()
    {
        gdd.homeDirectory = null;
        fl = new Fontlist();
        fl.initialize( null );
    }

    /**
     * Because this test takes a long time to run, we put all the asserts in this one test, so that it's only run once.
     */
    @Test
    public void verifyGeneratedFiles()
    {
        String filename =  "./testFontList.txt";

        fl = new Fontlist();
        fl.initialize( gdd );
        fl.create( filename );

        FontDirectoriesEntries fde = fl.getFontDirectories();
        assertTrue( fde.size() > 5 );      // value of 5 chosen as reasonable minimum, even for a test system

        Collection<String> files = fl.getFilenames();
        assertTrue( files.size() > 10 );   // value of 10 chosen as reasonable minimum, even for a test system

        TreeMap<String, List<String >> fontAndFamilies = fl.getFontFamilies();
        assertTrue( fontAndFamilies.size() > 10 );   // value of 10 chosen as reasonable minimum, even for a test system

        // count the number of lines in the fontlist report
        File f = new File( filename );
        int lineCount = 0;
        try
        {
            final List<String> lines = Files.readLines(f, Charset.defaultCharset());
            for (final String line : lines)
                lineCount++;
        }
        catch (IOException ioEx)
        {
            fail( "ERROR trying to retrieve lines from file: " + filename + "-" + ioEx.toString());
        }

        assertTrue( lineCount >= 10 );  // value of 10 chosen as reasonable minimum, even for a test system
        // get rid of the file
        f.delete();
    }
}
