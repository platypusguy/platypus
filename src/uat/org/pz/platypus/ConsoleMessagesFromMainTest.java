/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertTrue;

/**
 * @author alb
 */
public class ConsoleMessagesFromMainTest
{
    @Test
    public void mainPrintsCopyrightEvenWhenErrorOccurs()
    {
        PrintStream originalStdout = System.out;

        // capture stdout
        OutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream( os );
        System.setOut( ps );

        // pass it null for args, which triggers an NPE.
        Main main = new Main();
        try {
            main.run( null );
        }
        catch( Throwable t ) {};

        String output = os.toString();
        assertTrue( output.startsWith("Platypus v. "));

        // restore stdout
        System.setOut( originalStdout );
    }
}
