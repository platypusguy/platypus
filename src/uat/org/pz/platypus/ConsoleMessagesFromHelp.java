/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertTrue;

/**
 * Test whether -help includes usage instructions.
 * @author alb
 */
public class ConsoleMessagesFromHelp
{
    @Test
    public void mainPrintsCopyrightEvenWhenErrorOccurs()
    {
        PrintStream originalStdout = System.out;

        // capture stdout
        OutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream( os );
        System.setOut( ps );

        String[] args = { "-home", ".", "-help" };

        Main main = new Main();
        main.run( args );
        String output = os.toString();
        assertTrue( output.startsWith("Platypus v. "));
        assertTrue( output.contains( "Usage:" ));

        // restore stdout
        System.setOut( originalStdout );
    }
}
