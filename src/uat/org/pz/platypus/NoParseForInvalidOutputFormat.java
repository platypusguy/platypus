/**
 *  Platypus: Page Layout and Typesetting Software (free at platypus.pz.org)
 *
 *  Platypus is (c) Copyright 2009-11 Pacific Data Works LLC. All Rights Reserved.
 *  Licensed under Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
 */
package org.pz.platypus;

import org.junit.Test;
import org.pz.platypus.Platypus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * An invalid output format should result in no parsing of input tokens.
 * @author alb
 */
public class NoParseForInvalidOutputFormat
{
    @Test
    public void testValidTestSeq001()
    {
        String testName = "NoParseForInvalidOutputFormat";

        PrintStream originalStdout = System.out;

        // capture stdout
        OutputStream os = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream( os );
        System.setOut( ps );

        // create the input file
        String contents = "[+i]Text[-i][]";
        byte[] content = contents.getBytes();

        try {
            com.google.common.io.Files.write( content, new File( testName + ".txt" ));
        }
        catch( Throwable t ) {
            fail( "Unable to create an input file in " + testName );
        }

        // ask for an unsupported (therefore, invalid) output format
        String[] args = {  testName + ".txt", testName + ".mia", "-vverbose" };
        Platypus platy = new Platypus();
        platy.main( args );

        // validate that no input tokens were parsed.
        String output = os.toString();
        assertTrue( output.contains("Token list is empty."));

        // restore stdout
        System.setOut(originalStdout);

        // delete bogus input file
        new File( testName + ".txt" ).delete();
    }
}
